
DELETE FROM public.account_type;
DELETE FROM public.product_status;
DELETE FROM public.product_source;
DELETE FROM public.product_class;
DELETE FROM public.product_group;

insert into public.account_type values
(DEFAULT, 'None'),
(DEFAULT, 'Expense'),
(DEFAULT, 'Revenue'),
(DEFAULT, 'Asset'),
(DEFAULT, 'Liability');

insert into public.product_status values
(DEFAULT, 'Obsolete'),
(DEFAULT, 'Started'),
(DEFAULT, 'Active'),
(DEFAULT, 'Unknown');

insert into public.product_source values
(DEFAULT,'Make'),
(DEFAULT,'Stock'),
(DEFAULT,'Buy'),
(DEFAULT,'Surrogate');

insert into public.product_class values
(DEFAULT, 'RM', 'RAW MATERIALS'),
(DEFAULT, 'R1', 'RUBBER TIRED WHEELS STANDARD'),
(DEFAULT, 'R2', 'RUBBER TIRED WHEELS SPECIAL'),
(DEFAULT, 'P1', 'POLY TIRED WHEELS STANDARD'),
(DEFAULT, 'P2', 'POLY TIRED WHEELS SPECIAL'),
(DEFAULT, 'S1', 'SEMI STEEL AND FORGED STANDARD'),
(DEFAULT, 'S2', 'SEMI STEEL AND FORGED SPECIAL'),
(DEFAULT, 'SP', 'SOLID POLYURETHANE WHEELS'),
(DEFAULT, 'TO', 'TREAD ONLY'),
(DEFAULT, 'MI', 'MISC RETREAD'),
(DEFAULT, 'EQ', 'IN PLANT MACHINERY'),
(DEFAULT, 'MP', 'MAINTANENCE PARTS AND EQUIPMENT REPLACEMENT PARTS');

insert into public.product_group values
(DEFAULT, '3', 'BAR STOCK STEEL'),
(DEFAULT, '4', 'SPECIAL IRON CASTINGS RAW IRON'),
(DEFAULT, '5', 'STANDARD IRON CASTINGS RAW IRON'),
(DEFAULT, '6', 'STANDARD FORGED STEEL CASTINGS RAW STEEL'),
(DEFAULT, '7', 'ALUMINUM CASTINGS'),
(DEFAULT, '8', 'SEALS AND WASHERS'),
(DEFAULT, '9', 'SPANNERS AND BUSHINGS'),
(DEFAULT, '11', 'ROLLER BEARINGS - ALL'),
(DEFAULT, '12', 'TAPERED ROLLER BEARINGS AND RELATED PARTS'),
(DEFAULT, '13', 'BALL BEARINGS & MISC BEARINGS'),
(DEFAULT, '14', 'MISC. HARDWARE_ GREASE FTGS_ SET SCREWS_ ETC..'),
(DEFAULT, '15', 'WELDED STEEL RINGS AS BASE BANDS FOR PRESS-ON TIRES'),
(DEFAULT, '1A', 'ALUMINUM_ MACHINED'),
(DEFAULT, '1F', 'FORGED STEEL_ MACHINED'),
(DEFAULT, '1I', 'IRON_ MACHINED'),
(DEFAULT, '1S', 'SEMI STEEL_ MACHINED'),
(DEFAULT, '1Z', 'ZINC ALLOY_ MACHINED'),
(DEFAULT, '23', 'POLYURETHANE AND CURATIVE'),
(DEFAULT, '24', 'PIGMENTS AND DYES'),
(DEFAULT, '25', 'MOLD COST'),
(DEFAULT, '26', 'BONDING AGENT_ MOLD RELEASE_ PLASTICIZER'),
(DEFAULT, '27', 'METAL CUTTING FLUID'),
(DEFAULT, '28', 'PAINT'),
(DEFAULT, 'AS', 'STANDARD POLY ON ALUMINUM_ FINISHED'),
(DEFAULT, 'AW', 'STANDARD POLY ON ALUMINUM_ TRIMMED'),
(DEFAULT, 'AX', 'SPECIAL POLY ON ALUMINUM_ FINISHED'),
(DEFAULT, 'DS', 'DUALTHANE_ FINISHED'),
(DEFAULT, 'DT', 'DUALTHANE_ TRIMMED'),
(DEFAULT, 'DX', 'SPECIAL DUALTHANE_ FINISHED'),
(DEFAULT, 'FS', 'FORGED STEEL_ FINISHED'),
(DEFAULT, 'FW', 'STANDARD POLY ON FORGED STEEL_ TRIMMED'),
(DEFAULT, 'HS', 'POLY ON HEAVY DUTY IRON_ FINISHED'),
(DEFAULT, 'HW', 'POLY ON HEAVY DUTY IRON_ TRIMMED'),
(DEFAULT, 'HX', 'SPECIAL POLY ON HEAVY DUTY IRON_ FINISHED'),
(DEFAULT, 'IS', 'STANDARD POLY ON IRON_ FINISHED'),
(DEFAULT, 'IW', 'STANDARD POLY ON IRON_TRIMMED'),
(DEFAULT, 'IX', 'SPECIAL POLY ON IRON_ FINISHED'),
(DEFAULT, 'JW', 'SPECIAL POLY ON IRON TRIMMED'),
(DEFAULT, 'MS', 'MISCELLANEOUS POLYURETHANE_ FINISHED'),
(DEFAULT, 'NS', 'SOLID POLYURETHANE_ FINISHED'),
(DEFAULT, 'NW', 'SOLID POLYURETHANE_ TRIMMED'),
(DEFAULT, 'NX', 'SPECIAL SOLID POLYURETHANE_ FINISHED'),
(DEFAULT, 'OD', 'MOLD DUALTHANE CENTER'),
(DEFAULT, 'RS', 'RUBBER TIRED_ FINISHED'),
(DEFAULT, 'RW', 'RUBBER TIRED_ TRIMMED'),
(DEFAULT, 'RX', 'SPECIAL RUBBER TIRED_ FINISHED'),
(DEFAULT, 'SS', 'SEMI STEEL_ FINISHED'),
(DEFAULT, 'SX', 'SPECIAL SEMI STEEL_ FINISHED'),
(DEFAULT, 'TO', 'TREAD ONLY_ TRIMMED'),
(DEFAULT, 'TS', 'TREAD ONLY_ FINISHED'),
(DEFAULT, 'TX', 'SPECIAL TREAD ONLY_ FINISHED'),
(DEFAULT, '100', 'FABRICATED MACHINARY OR ASSEMBLIES'),
(DEFAULT, 'MA', 'MAINTANENCE');
