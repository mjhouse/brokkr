package settings;


import core.controllers.BaseController;
import core.database.DatabaseManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import settings.models.Setting;
import settings.models.Settings;

import java.util.ArrayList;

/**
 * The controller for the Settings workspace.
 */
public class SettingsController extends BaseController {

    /**
     * The TableView used to display general settings.
     */
    @FXML
    private TableView<Setting> settingsTable;

    /**
     * The values column of the settingsTable.
     */
    @FXML
    private TableColumn<Setting, String> valuesColumn;

    /**
     * The textfield for the uri connection string.
     */
    @FXML
    private TextField uriField;

    /**
     * The textfield for the username.
     */
    @FXML
    private TextField usernameField;

    /**
     * The textfield for the password.
     */
    @FXML
    private TextField passwordField;

    public SettingsController(){
        super("/views/workspaces/settings_workspace.fxml");
    }

    /**
     * Initial setup of the workspace.
     */
    @FXML
    protected void initialize()
    {
        Settings settings = Settings.getInstance();

        uriField.setText(settings.getConfig("uri"));
        usernameField.setText(settings.getConfig("username"));
        passwordField.setText(settings.getConfig("password"));

        ObservableList<Setting> data = getSettings();
        settingsTable.setItems(data);

        valuesColumn.setOnEditCommit(this::onCellEdit);

    }

    /**
     * Get a collection of Setting objects.
     *
     * @return an observable list of Setting objects.
     */
    private ObservableList<Setting> getSettings()
    {
        ArrayList<Setting> values = Settings.getInstance().getSettings();
        return FXCollections.observableList(values);
    }

    /**
     * Handler for the save button click event.
     *
     * @param event the event object.
     */
    @FXML
    @SuppressWarnings("unused")
    public void handleSave(ActionEvent event)
    {
        String database = uriField.getText();
        String username = usernameField.getText();
        String password = passwordField.getText();

        Settings settings = Settings.getInstance();
        DatabaseManager dbmgr = DatabaseManager.getInstance();

        if (dbmgr.testCredentials(database, username, password)) {
            settings.putConfig("username", username);
            settings.putConfig("password", password);
            settings.putConfig("uri", database);
            info("valid credentials set for: " + database);
        } else {
            warning("could not connect to: " + database);
        }

        for (Setting s : settingsTable.getItems()) {
            String key = s.getKey();
            String value = s.getValue();
            System.out.println(key + " : " + value);
            if (!value.equals(settings.getString(key))) {
                settings.putSetting(s);
            }
        }
    }

    /**
     * Handler for the cell edit event.
     *
     * @param event the event object.
     */
    private void onCellEdit(TableColumn.CellEditEvent<Setting, String> event)
    {
        Setting setting = event.getTableView().getItems().get(event.getTablePosition().getRow());
        setting.setValue(event.getNewValue());
    }

}
