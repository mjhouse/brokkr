package settings.models;

import core.communication.interfaces.Loggable;

import java.util.ArrayList;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;

import static java.util.Map.entry;

/**
 * A manager for application settings.
 */
public class Settings implements Loggable {

    /**
     * A hashmap of default settings.
     */
    private static final Map<String, String> defaults = Map.ofEntries(entry("Debug", "False"), entry("Theme", "Default"), entry("StartWorkspace", "Empty"), entry("BatchSize", "20"), entry("DisplayErrors", "Warning"));
    /**
     * The singleton instance of the Settings object.
     */
    private static Settings instance = null;

    private Preferences root;

    /**
     * A Preference object referencing internal
     * (not-user-set) configuration.
     */
    private Preferences config;
    /**
     * A Preference object referencing user-facing
     * settings.
     */
    private Preferences settings;

    /**
     * The empty constructor inits the Preferences instances
     * and inserts default values where the user hasn't created
     * them.
     */
    private Settings(String namespace)
    {
        root = Preferences.userRoot().node(namespace);
        config = root.node("config");
        settings = root.node("settings");

        setDefaults();
    }

    /**
     * Get the singleton instance of Settings.
     *
     * @param namespace the preferences root node
     * @return the Settings object.
     */
    public static Settings getInstance(String namespace)
    {
        if (instance == null)
            instance = new Settings(namespace);

        return instance;
    }

    /**
     * Get the singleton instance of Settings.
     *
     * @return the Settings object.
     */
    public static Settings getInstance()
    {
        return getInstance("brokkr");
    }

    /**
     * Delete the instance of Settings
     * @return true if delete operation was successful
     */
    public boolean deleteInstance(){
        try {
            config = null;
            settings = null;
            instance = null;

            root.removeNode();
            root = null;

            return true;
        }
        catch(BackingStoreException e){
            return false;
        }
    }

    /**
     * Generate default values when not set by the user.
     */
    private void setDefaults()
    {
        for (Map.Entry<String, String> entry : defaults.entrySet()) {
            if (getString(entry.getKey()).isEmpty()) {
                putString(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * Get a config value (not a user setting)
     *
     * @param key the key to fetch.
     * @return the value of the config setting.
     */
    public String getConfig(String key)
    {
        return config.get(key, "");
    }

    /**
     * Set a config value.
     *
     * @param key   the key to set.
     * @param value the value to set the setting to.
     */
    public void putConfig(String key, String value)
    {
        config.put(key, value);
    }

    /**
     * Get a setting as a string.
     *
     * @param key the key to fetch.
     * @return the value of the setting as a string.
     */
    public String getString(String key)
    {
        return settings.get(key, "");
    }

    /**
     * Set a String value to a particular setting.
     *
     * @param key   the key to set.
     * @param value the value to set.
     */
    @SuppressWarnings("WeakerAccess")
    public void putString(String key, String value)
    {
        settings.put(key, value);
    }

    /**
     * Get a setting as a Setting object.
     *
     * @param key the key to get.
     * @return the key/value pair as a Setting object.
     */
    @SuppressWarnings("unused")
    public Setting getSetting(String key)
    {
        String value = getString(key);
        return new Setting(key, value);
    }

    /**
     * Set a value as a Setting object.
     *
     * @param setting the setting object to store.
     */
    public void putSetting(Setting setting)
    {
        putString(setting.getKey(), setting.getValue());
    }

    /**
     * Get all settings as an array of Setting objects.
     *
     * @return an ArrayList of Setting objects.
     */
    public ArrayList<Setting> getSettings()
    {
        ArrayList<Setting> results = new ArrayList<>();

        try {
            for (String key : settings.keys()) {
                String value = getString(key);
                results.add(new Setting(key, value));
            }
        } catch (BackingStoreException e) {
            System.out.println("error on settings load");
            e.printStackTrace();
        }

        return results;
    }

    /**
     * Subscribe to changed-setting events.
     *
     * @param setting  the setting to subscribe to.
     * @param listener the listener to notify.
     */
    public void listen(String setting, PreferenceChangeListener listener)
    {
        settings.addPreferenceChangeListener((event) -> {
            if (event.getKey().equals(setting))
                listener.preferenceChange(event);
        });
    }
}
