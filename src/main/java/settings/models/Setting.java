package settings.models;

import core.communication.interfaces.Loggable;

/**
 *
 */
public class Setting implements Loggable {

    /**
     * The setting key.
     **/
    private String key;

    /**
     * The setting value.
     **/
    private String value;

    /**
     * Create a new setting object given a key and value.
     *
     * @param key the key to set for the setting.
     * @param val the value to set for the setting.
     */
    public Setting(String key, String val)
    {
        this.key = key;
        this.value = val;
    }

    /**
     * Get the key for this setting.
     *
     * @return the key for this setting.
     */
    public String getKey()
    {
        return key;
    }

    /**
     * Set the key for this setting.
     *
     * @param key the key to set.
     */
    public void setKey(String key)
    {
        this.key = key;
    }

    /**
     * Get the value of this setting.
     *
     * @return the value of this setting.
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Set the value of this setting.
     *
     * @param value the value to set.
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * Get the value as a boolean primitive.
     *
     * @return the value of the setting as a bool.
     */
    public boolean asBool()
    {
        return Boolean.parseBoolean(value);
    }

    /**
     * Get the value of the setting as a string.
     *
     * @return the value of the setting as a string.
     */
    public String asString()
    {
        return value;
    }
}
