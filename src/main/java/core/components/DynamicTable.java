package core.components;

import core.models.BaseModel;
import core.models.properties.BaseProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DynamicTable<T extends BaseModel> extends TableView<T> {

    private List<String> blacklist;

    public DynamicTable(){
        super();

        blacklist = new ArrayList<>();
        setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    public void ignoreProperties(String... blacklist){
        this.blacklist.clear();
        this.blacklist.addAll(Arrays.asList(blacklist));
    }

    public void setContent(List<T> objects){
        if(objects.size() > 0 && needColumnCreation(objects.get(0))) {
            getColumns().clear();
            if (objects.size() > 0) {
                setColumns(objects.get(0).propertyNames());
            }
        }
        setItems(FXCollections.observableList(objects));
    }

    private boolean needColumnCreation(T object){

        List<String> headers = object.propertyNames();
        boolean needCreation = false;

        if(getColumns().size() < 1)
            needCreation = true;

        for(TableColumn column : getColumns()){
            String label = column.getText();
            if(!headers.contains(label))
                needCreation = true;
        }

        return needCreation;
    }

    private void setColumns(List<String> headers){
        for(String header : headers){

            if(blacklist.contains(header))
                continue;

            TableColumn<T,String> column = new TableColumn<>(header);
            getColumns().add(column);

            column.setCellValueFactory(param -> {
                String name = param.getTableColumn().getText();
                BaseProperty value= param.getValue().propertyValue(name);
                return new ReadOnlyObjectWrapper<>(value.getStringValue());
            });
        }
    }


}
