package core.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.paint.Paint;
import javafx.scene.shape.SVGPath;
import org.kordamp.ikonli.javafx.FontIcon;

public class GraphicButton extends Button {

    public GraphicButton(String iconid)
    {
        super();
        setGraphic(new FontIcon(iconid));
    }

    public GraphicButton onClick(EventHandler<ActionEvent> value)
    {
        this.setOnAction(value);
        return this;
    }

}
