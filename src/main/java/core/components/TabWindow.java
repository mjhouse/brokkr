package core.components;

import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class TabWindow extends Window {

    private Tab tab;

    private TabPane parent;

    private int index;

    public TabWindow(Tab t)
    {
        super();
        tab = t;
        parent = t.getTabPane();
        index = parent.getTabs().indexOf(tab);

        AnchorPane pane = (AnchorPane) tab.getContent().lookup("AnchorPane");

        if (pane != null) {
            display(pane,parent.getWidth(),parent.getHeight());
            parent.getTabs().remove(tab);
        }
    }

    @Override
    protected void onClose(WindowEvent event)
    {
        AnchorPane pane = (AnchorPane) scene.lookup("AnchorPane");
        if (pane != null) {
            tab.setContent(pane);
            parent.getTabs().add(index, tab);
            parent.getSelectionModel().select(index);
        }
    }

}
