package core.components;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Window extends Stage {

    protected Scene scene;

    protected Parent box;

    public Window() {
    }

    private Parent getBox(Node content)
    {
        box = new VBox(content);

        AnchorPane.setTopAnchor(box, 0.0);
        AnchorPane.setRightAnchor(box, 0.0);
        AnchorPane.setBottomAnchor(box, 0.0);
        AnchorPane.setLeftAnchor(box, 0.0);

        return box;
    }

    protected void display(Node content, double width, double height){
        box = getBox(content);
        scene = new Scene(box, width, height);

        this.setOnCloseRequest(this::onClose);
        this.setOnHidden(this::onHidden);
        this.setOnShown(this::onShown);

        setScene(scene);
    }

    protected void onShown(WindowEvent windowEvent)
    {
    }

    protected void onHidden(WindowEvent windowEvent)
    {
    }

    protected void onClose(WindowEvent event)
    {

    }

}
