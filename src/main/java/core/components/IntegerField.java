package core.components;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import java.util.regex.Pattern;

public class IntegerField extends TextField {

    private final ObservableValue<Integer> shadow;

    final String Digits = "(\\p{Digit}+)";

    public IntegerField(){
        super();
        shadow = new SimpleObjectProperty<>();
        textProperty().addListener((obs,o,n) -> {
            Integer value = n.isEmpty() ? 0 : Integer.valueOf(n);
            ((SimpleObjectProperty<Integer>) shadow).set(value);
        });
    }

    private String withChange(int start, int end, String text){
        return getText().substring(0,start) + text + getText().substring(end);
    }

    @Override public void replaceText(int start, int end, String text) {
        String current = withChange(start,end,text);
        if (text.isEmpty() || Pattern.matches(Digits, current)) {
            super.replaceText(start, end, text);
        }
    }

    @Override public void replaceSelection(String text) {
        if (text.isEmpty() || Pattern.matches(Digits, text)) {
            super.replaceSelection(text);
        }
    }

    public Integer getValue(){
        return shadow.getValue();
    }

    public void setValue(Integer value){
        super.setText(value.toString());
    }

    public ObservableValue<? extends Integer> valueProperty(){
        return shadow;
    }

}
