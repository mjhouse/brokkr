package core.components;

import core.communication.interfaces.Loggable;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import settings.models.Settings;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Dialog extends AnchorPane implements Initializable, Loggable {

    private Stage stage;

    private Scene scene;

    private Parent box;

    private ButtonBar bar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public Dialog(String title, String path){
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            severe("Failed to load FXML file due to IOException",e);
            throw new RuntimeException(e);
        }

        bar = new ButtonBar();
        bar.setMaxHeight(30);
        bar.setPadding(new Insets(5,5,5,5));

        box = new VBox(this, bar);
        box.autosize();

        VBox.setVgrow(box,Priority.ALWAYS);
        VBox.setVgrow(this,Priority.ALWAYS);

        scene = new Scene(box);
        stage = new Stage();

        stage.setOnCloseRequest(this::onClose);
        stage.setOnHidden(this::onHidden);
        stage.setOnShown(this::onShown);

        stage.setScene(scene);
        stage.setTitle(title);

        stage.setResizable(false);
        setStyles(scene);
    }

    private void setStyles(Scene scene)
    {
        String name = Settings.getInstance().getString("Theme");
        try {
            String path = String.format("/themes/%s.css", name.toLowerCase());
            String external = getClass().getResource(path).toExternalForm();

            if (external != null && !external.isEmpty()) {
                Platform.runLater(() -> {
                    if (scene != null) {
                        ObservableList<String> styles = scene.getStylesheets();
                        if (styles != null) {
                            styles.clear();
                            styles.add(external);
                        }
                    }
                });
            }
        } catch (Exception e) {
            warning("invalid theme: " + name);
        }
    }

    public void sizeToScene(){
        stage.sizeToScene();
    }

    public void setResizable(boolean value){
        stage.setResizable(value);
    }

    public void show(){
        stage.show();
        stage.sizeToScene();
    }

    public void close(){
        stage.close();
    }

    protected void setBarPadding(double top, double right, double bottom, double left){
        bar.setPadding(new Insets(top,right,bottom,left));
    }

    protected void setBarPadding(double pad){
        bar.setPadding(new Insets(pad,pad,pad,pad));
    }

    protected void addButton(String label, ButtonBar.ButtonData data, EventHandler<ActionEvent> handler){
        Button btn = new Button(label);
        ButtonBar.setButtonData(btn, data);

        btn.setOnAction(handler);
        bar.getButtons().add(btn);
    }

    protected void onShown(WindowEvent windowEvent)
    {
    }

    protected void onHidden(WindowEvent windowEvent)
    {
    }

    protected void onClose(WindowEvent event)
    {
    }

}
