package core.components;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

import java.util.regex.Pattern;

public class DoubleField extends TextField {

    private final ObservableValue<Double> shadow;

    final String Digits = "(\\p{Digit}+)";

    final String DOUBLE =
                    ("[\\x00-\\x20]*"+  // Optional leading "whitespace"
                     "("+Digits+"(\\.)?("+Digits+"?))" +
                     "[\\x00-\\x20]*"); // Optional trailing "whitespace"

    public DoubleField(){
        super();
        shadow = new SimpleObjectProperty<>();
        textProperty().addListener((obs,o,n) -> {
            Double value = n.isEmpty() ? 0 : Double.valueOf(n);
            ((SimpleObjectProperty<Double>) shadow).set(value);
        });
    }

    private String withChange(int start, int end, String text){
        return getText().substring(0,start) + text + getText().substring(end);
    }

    @Override public void replaceText(int start, int end, String text) {
        String current = withChange(start,end,text);
        if (text.isEmpty() || Pattern.matches(DOUBLE, current)) {
            super.replaceText(start, end, text);
        }
    }

    @Override public void replaceSelection(String text) {
        if (text.isEmpty() || Pattern.matches(DOUBLE, text)) {
            super.replaceSelection(text);
        }
    }

    public Double getValue(){
        return shadow.getValue();
    }

    public ObservableValue<? extends Double> valueProperty(){
        return shadow;
    }

    public void setValue(Double value){
        super.setText(value.toString());
    }

}
