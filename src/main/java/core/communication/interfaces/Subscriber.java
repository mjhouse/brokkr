package core.communication.interfaces;

import core.communication.Broadcaster;
import core.communication.events.ControllerEvent;
import core.communication.flags.EventType;

public interface Subscriber {

    void onEvent(ControllerEvent event);

    default void sendEvent(EventType event)
    {
        Broadcaster.getInstance().broadcast(new ControllerEvent(this, event));
    }

    default void getEvent(EventType event)
    {
        Broadcaster.getInstance().subscribe(event, this);
    }

}
