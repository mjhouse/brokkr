package core.communication.interfaces;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import settings.models.Settings;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implements logging functions that display notifications and write to
 * STDERR when used.
 */
@SuppressWarnings("unused")
public interface Loggable {

    /**
     * Display a notification dialog.
     *
     * @param title   the title of the dialog.
     * @param header  the header of the dialog.
     * @param message the message to display.
     */
    default boolean confirm(String title, String header, String message)
    {
        Alert alert = new Alert( Alert.AlertType.WARNING,
                                 message,
                                 ButtonType.OK,
                                 ButtonType.CANCEL);
        alert.setTitle(title);
        alert.setHeaderText(header);

        Optional<ButtonType> result = alert.showAndWait();
        return result.isPresent() && (result.get() == ButtonType.OK);
    }

    /**
     * Display a notification dialog.
     *
     * @param title   the title of the dialog.
     * @param message the message to display.
     */
    default boolean confirm(String title, String message)
    {
        return confirm(title,message,"");
    }

    /**
     * Display a notification dialog.
     *
     * @param title   the title of the dialog.
     * @param header  the header of the dialog.
     * @param message the message to display.
     */
    default void display(String title, String header, String message)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Display a notification dialog.
     *
     * @param title   the title of the dialog.
     * @param header  the header of the dialog.
     * @param message the message to display.
     * @param level the requested log level
     */
    default void alert(String title, String header, String message, LogLevel level)
    {

        String current_name = Settings.getInstance().getSetting("DisplayErrors").asString();
        LogLevel current = LogLevel.fromString(current_name, LogLevel.NONE);

        if (!current.isNone() && level.toInt() <= current.toInt()) {
            display(title,header,message);
        }
    }

    /**
     * Display a notification dialog for an exception.
     *
     * @param message the message to display.
     * @param ex      the exception to display.
     */
    default void alert(String message, Exception ex, LogLevel level)
    {
        String current_name = Settings.getInstance().getSetting("DisplayErrors").asString();
        LogLevel current = LogLevel.fromString(current_name, LogLevel.SEVERE);

        if (!current.isNone() && level.toInt() <= current.toInt()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(ex.getClass().getSimpleName());
            alert.setHeaderText(ex.getMessage());
            alert.setContentText(message);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("The exception stacktrace was:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);


            alert.getDialogPane().setExpandableContent(expContent);
            alert.showAndWait();
        }
    }

    /**
     * Log a message at the SEVERE level.
     *
     * @param msg the message to log.
     */
    default void severe(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.SEVERE,cls,mtd,msg);
        alert("Brokkr", "Level: SEVERE", msg, LogLevel.SEVERE);
    }

    /**
     * Log a message and exception at the SEVERE level.
     *
     * @param msg the message to log.
     * @param ex  the exception to log.
     */
    default void severe(String msg, Exception ex)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.SEVERE,cls,mtd,msg,ex);
        alert(msg, ex, LogLevel.SEVERE);
    }

    /**
     * Log a message at the WARNING level.
     *
     * @param msg the message to log.
     */
    default void warning(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.WARNING,cls,mtd,msg);
        alert("Brokkr", "Level: WARNING", msg, LogLevel.WARNING);
    }

    default StackTraceElement getCaller(){
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        return stackTrace[3];
    }

    /**
     * Log a message and exception at the WARNING level.
     *
     * @param msg the message to log.
     * @param ex  the exception to log.
     */
    default void warning(String msg, Exception ex)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.WARNING,cls,mtd,msg,ex);
        alert(msg, ex, LogLevel.WARNING);
    }

    /**
     * Log a message at the INFO level.
     *
     * @param msg the message to log.
     */
    default void info(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.INFO,cls,mtd,msg);
        alert("Brokkr", "Level: INFO", msg, LogLevel.INFO);
    }

    /**
     * Log a message and exception at the INFO level.
     *
     * @param msg the message to log.
     * @param ex  the exception to log.
     */
    default void info(String msg, Exception ex)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.INFO,cls,mtd,msg,ex);
        alert(msg, ex, LogLevel.INFO);
    }

    /**
     * Log a message at the CONFIG level.
     *
     * @param msg the message to log.
     */
    default void config(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.CONFIG,cls,mtd,msg);
        alert("Brokkr", "Level: CONFIG", msg, LogLevel.CONFIG);
    }

    /**
     * Log a message and exception at the CONFIG level.
     *
     * @param msg the message to log.
     * @param ex  the exception to log.
     */
    default void config(String msg, Exception ex)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.CONFIG,cls,mtd,msg,ex);
        alert(msg, ex, LogLevel.CONFIG);
    }

    /**
     * Log a message at the 'fine' debug level.
     *
     * @param msg the message to log.
     */
    default void fine(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.FINE,cls,mtd,msg);
        alert("Brokkr", "Level: FINE", msg, LogLevel.FINE);
    }

    /**
     * Log a message at the 'finer' debug level.
     *
     * @param msg the message to log.
     */
    default void finer(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.FINER,cls,mtd,msg);
        alert("Brokkr", "Level: FINER", msg, LogLevel.FINER);
    }

    /**
     * Log a message at the 'finest' debug level.
     *
     * @param msg the message to log.
     */
    default void finest(String msg)
    {
        StackTraceElement caller = getCaller();
        String cls = caller.getClassName();
        String mtd = caller.getMethodName();
        Logger logger = Logger.getLogger(cls);

        logger.logp(Level.FINEST,cls,mtd,msg);
        alert("Brokkr", "Level: FINEST", msg, LogLevel.FINEST);
    }

    enum LogLevel {
        NONE("none"), SEVERE("severe"), WARNING("warning"), INFO("info"), CONFIG("config"), FINE("fine"), FINER("finer"), FINEST("finest");

        private String name;

        LogLevel(String value)
        {
            name = value;
        }

        public static LogLevel fromString(String value, LogLevel alternate)
        {
            LogLevel result = fromString(value);
            if (result != null)
                return result;
            else
                return alternate;
        }

        public static LogLevel fromString(String value)
        {
            try {
                return valueOf(value.toUpperCase());
            } catch (Exception e) {
                return null;
            }
        }

        public String toString()
        {
            return name;
        }

        public int toInt()
        {
            return ordinal();
        }

        public boolean hasValue(String other)
        {
            return name.equals(other.toLowerCase());
        }

        public boolean isNone()
        {
            return (this == LogLevel.NONE);
        }

    }

}
