package core.communication.flags;

public enum EventType {
    ALL, LOAD, REQUEST_INIT
}
