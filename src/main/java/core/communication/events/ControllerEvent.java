package core.communication.events;

import core.communication.flags.EventType;
import core.communication.interfaces.Subscriber;

import java.util.EventObject;

public class ControllerEvent extends EventObject {

    private final EventType type;

    public ControllerEvent(Subscriber sender, EventType event)
    {
        super(sender);
        type = event;
    }

    public EventType getType()
    {
        return type;
    }

}
