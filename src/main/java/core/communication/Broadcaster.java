package core.communication;

import core.communication.events.ControllerEvent;
import core.communication.flags.EventType;
import core.communication.interfaces.Subscriber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Broadcaster {

    private static Broadcaster instance;
    private HashMap<EventType, ArrayList<Subscriber>> subscribers;

    private Broadcaster()
    {
        subscribers = new HashMap<>();
        for (EventType e : EventType.values()) {
            subscribers.put(e, new ArrayList<>());
        }
    }

    public static Broadcaster getInstance()
    {
        if (instance == null)
            instance = new Broadcaster();
        return instance;
    }

    public void subscribe(Subscriber controller)
    {
        subscribers.get(EventType.ALL).add(controller);
    }

    public void subscribe(EventType event, Subscriber controller)
    {
        subscribers.get(event).add(controller);
    }

    public void broadcast(ControllerEvent event)
    {
        List<Subscriber> receivers = Stream.concat(
                subscribers.get(event.getType()).stream(),
                subscribers.get(EventType.ALL).stream())
                .collect(Collectors.toList());

        for (Subscriber c : receivers) {
            if (c != event.getSource())
                c.onEvent(event);
        }
    }

}
