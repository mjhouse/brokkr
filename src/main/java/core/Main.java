package core;

import core.database.DatabaseManager;
import core.models.properties.DoubleProperty;
import products.models.Product;

/**
 * Main class that acts as an entry point to the application.
 */
public class Main {
    /**
     * Entry point to the application.
     *
     * @param args command-line arguments.
     */
    static public void main(String[] args)
    {
        Brokkr view = new Brokkr();
        view.run(args);
    }

}
