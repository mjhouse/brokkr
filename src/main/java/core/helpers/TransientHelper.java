package core.helpers;

import core.communication.interfaces.Loggable;
import javafx.scene.chart.XYChart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TransientHelper implements Loggable {

    private static File getFile(String path)
    {

        return ResourceHelper.getInstance().getFile(path);
    }

    private static BufferedReader getBufferedReader(String path)
    {
        BufferedReader reader = null;
        try {
            File file = getFile(path);
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ignored) {
        }
        return reader;
    }

    public static XYChart.Series<LocalDateTime, Number> generateTestData()
    {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        XYChart.Series<LocalDateTime, Number> results = new XYChart.Series<>();

        String text;
        try {
            BufferedReader reader = TransientHelper.getBufferedReader("/test/sample_usage.csv");
            if (reader != null) {
                while ((text = reader.readLine()) != null) {
                    String[] result = text.split(",");
                    if (result.length >= 2) {
                        double usage = Integer.parseInt(result[1]);
                        LocalDateTime date = LocalDateTime.parse(result[0], format);
                        results.getData().add(new XYChart.Data<>(date, usage));
                    }
                }
            }
        } catch (Exception ignored) {
        }

        return results;
    }

}
