package core.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

public class DateTimeHelper {

    private static ZoneOffset getZoneOffset()
    {
        ZoneId tz = ZoneId.systemDefault();
        return tz.getRules().getOffset(Instant.now());
    }

    public static Long getTimestamp(LocalDateTime dt)
    {
        return dt.toEpochSecond(getZoneOffset());
    }

    public static LocalDateTime getLocalDateTime(Long ts)
    {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(ts), ZoneId.systemDefault());
    }

    public static LocalDateTime getLocalDateTime(String ts){
        return LocalDateTime.parse(ts,ISO_OFFSET_DATE_TIME);
    }

    public static String getTimeStamp(){
        return ZonedDateTime.now().format(ISO_OFFSET_DATE_TIME);
    }

}
