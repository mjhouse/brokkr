package core.helpers;

import core.Brokkr;
import javafx.scene.text.Font;

import java.io.File;

public class ResourceHelper {

    public static final String RIGHT_ARROW = "M15,18, 21,12 ,15,6,17,6, 23,12 ,17,18";
    public static final String LEFT_ARROW = "M15,18,9,12,15,6,17,6,11,12,17,18";
    public static final String SQUARE = "M10,20,20,20,20,10,10,10,10,20";


    //region Static Resources
    public static final String TRANSPARENT = "rgba(0,0,0,0)";
    public static final String BLACK = "rgba(0,0,0,1)";

    public static final Font FONT = Font.loadFont(Brokkr.class.getResourceAsStream("/fa/fontawesome-webfont.ttf"),10);

    private static ResourceHelper instance;

    public static ResourceHelper getInstance()
    {
        if (instance == null)
            instance = new ResourceHelper();
        return instance;
    }

    public File getFile(String path)
    {
        return new File(getClass().getResource(path).getFile());
    }

    //endregion Static Resources

}
