package core;

import core.communication.interfaces.Loggable;
import core.controllers.MainController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import settings.models.Settings;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * The application.
 */
public class Brokkr extends Application implements Loggable {

    /**
     * The singular scene object.
     **/
    private static Scene scene;

    /**
     * The singular stage object.
     */
    private static Stage stage;

    /**
     * Get the current scene.
     *
     * @return the current scene.
     */
    public static Scene getScene()
    {
        return scene;
    }

    /**
     * Get the current stage.
     *
     * @return the current stage.
     */
    public static Stage getStage()
    {
        return stage;
    }

    /**
     * Called to launch the application.
     *
     * @param given the windowing context.
     */
    @Override
    public void start(Stage given)
    {
        FXMLLoader loader = new FXMLLoader();
        warning("boop");


        initStateFromSettings();
        initSettingsHandlers();

        stage = given;

        MainController main = new MainController();
        scene = new Scene((Parent)main.getRoot(), 800, 600);

        stage.setTitle("Brokkr");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/brokkr.png")));

        stage.setScene(scene);
        stage.show();
    }

    /**
     * Sets the application state from saved settings, or from defaults
     * supplied by the 'Settings' instance.
     */
    private void initStateFromSettings()
    {
        setStylesheet(Settings.getInstance().getString("Theme"));
    }

    /**
     * Initializes handlers for settings changed at runtime.
     */
    private void initSettingsHandlers()
    {
        Settings settings = Settings.getInstance();

        settings.listen("Debug", (event) -> {
            // set debug logging
        });

        settings.listen("Theme", (event) -> {
            String value = event.getNewValue();
            setStylesheet(value);
        });

        settings.listen("StartWorkspace", (event) -> {
            // set initial workspace
        });

    }

    /**
     * Change the style sheet while the application is running.
     *
     * @param name the name of the stylesheet to load.
     */
    private void setStylesheet(String name)
    {
        try {
            String path = String.format("/themes/%s.css", name.toLowerCase());
            String external = getClass().getResource(path).toExternalForm();

            if (external != null && !external.isEmpty()) {
                Platform.runLater(() -> {
                    if (scene != null) {
                        ObservableList<String> styles = scene.getStylesheets();
                        if (styles != null) {
                            styles.clear();
                            styles.add(external);
                        }
                    }
                });
            }
        } catch (Exception e) {
            warning("invalid theme: " + name);
        }
    }

    /**
     * Run the BRokkr application.
     *
     * @param args command-line arguments.
     */
    void run(String[] args)
    {
        launch(args);
    }


}
