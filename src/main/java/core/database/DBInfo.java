package core.database;

import core.communication.interfaces.Loggable;
import settings.models.Settings;

import java.util.Map;

import static java.util.Map.entry;

/**
 * Represents information related to a particular type of
 * database connection.
 */
public class DBInfo implements Loggable {

    /**
     * A static collection of database configurations.
     */
    private static final Map<String, DBInfo> configurations = Map.ofEntries(
            entry("postgresql", new DBInfo("org.postgresql.Driver", "org.hibernate.dialect.PostgreSQL82Dialect")));
    /**
     * The parsed URI.
     */
    private String uri;
    /**
     * The driver for a particular database type.
     */
    private String driver;
    /**
     * The SQL dialect for a particular database type.
     */
    private String dialect;

    /**
     * The private constructor for a DBInfo instance.
     *
     * @param driver  the driver type as a string.
     * @param dialect the SQL dialect as a string.
     */
    private DBInfo(String driver, String dialect)
    {
        this.uri = "";
        this.driver = driver;
        this.dialect = dialect;
    }

    /**
     * Parse a connection uri to determine the driver and dialect to use.
     *
     * @param uri the uri to parse.
     * @return a DBInfo instance with configuration info.
     */
    public static DBInfo parse(String uri)
    {
        uri = uri.toLowerCase();
        DBInfo info = null;

        String[] result = uri.split(":");
        if (result.length > 1 && configurations.containsKey(result[0])) {
            info = configurations.get(result[0]);
            info.setUri(uri);
        }

        return info;
    }

    /**
     * A wrapper to access the current username from Settings.
     *
     * @return the username as a String.
     */
    public String getUsername()
    {
        return Settings.getInstance().getConfig("username");
    }

    /**
     * A wrapper to access the current password from Settings.
     *
     * @return the password as a String.
     */
    public String getPassword()
    {
        return Settings.getInstance().getConfig("password");
    }

    /**
     * Get the URI as a string.
     *
     * @return the String uri.
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * Set the uri for this DBInfo instance.
     *
     * @param uri the uri to set.
     */
    public void setUri(String uri)
    {
        this.uri = uri;
    }

    /**
     * Get the prepended JDBC uri.
     *
     * @return the fully JDBC uri.
     */
    public String getFullUri()
    {
        return "jdbc:" + uri;
    }

    /**
     * Get the driver as a String.
     *
     * @return the driver name as a String.
     */
    public String getDriver()
    {
        return driver;
    }

    /**
     * Set the driver for this DBInfo instance.
     *
     * @param driver the driver to set.
     */
    @SuppressWarnings("unused")
    public void setDriver(String driver)
    {
        this.driver = driver;
    }

    /**
     * Get the dialect.
     *
     * @return the dialect as a String.
     */
    public String getDialect()
    {
        return dialect;
    }

    /**
     * Set the dialect for this DBInfo instance.
     *
     * @param dialect the dialect as a String.
     */
    @SuppressWarnings("unused")
    public void setDialect(String dialect)
    {
        this.dialect = dialect;
    }

}
