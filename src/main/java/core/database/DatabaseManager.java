package core.database;

import core.communication.interfaces.Loggable;
import core.models.BaseModel;
import financial.models.Account;
import financial.models.AccountType;
import financial.models.Company;
import javafx.util.Pair;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import products.components.AutoComboBox;
import products.models.*;
import settings.models.Settings;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A manager that handles database interaction and configuration.
 */
public class DatabaseManager implements Loggable, AutoComboBox.DataSource {

    /**
     * The singleton instance of DatabaseManager.
     */
    private static DatabaseManager instance;

    /**
     * A SessionFactory to create new sessions with the database.
     */
    private SessionFactory factory;

    /**
     * The private constructor of the DatabaseManager. Attempts to
     * create the SessionFactory.
     */
    private DatabaseManager()
    {
        if ((factory = createSessionFactory()) == null) {
            warning("database connection couldn't be made at init");
        }
    }

    /**
     * Get the instance of DatabaseManager.
     *
     * @return the singleton instance of DatabaseManager.
     */
    public static DatabaseManager getInstance()
    {
        if (instance == null)
            instance = new DatabaseManager();

        return instance;
    }

    /**
     * Try to create the SessionFactory.
     *
     * @param uri  the uri to connect to.
     * @param user the username to connect with.
     * @param pass the password to connect with.
     * @return a SessionFactory or null on failure.
     */
    private SessionFactory createSessionFactory(String uri, String user, String pass)
    {
        SessionFactory factory = null;

        if (!user.isEmpty() && !uri.isEmpty()) {

            try {
                Configuration conf = new Configuration().configure(
                        getClass().getResource("/mapping/postgres-hibernate.cfg.xml"))
                        .addAnnotatedClass(Product.class)
                        .addAnnotatedClass(ProductClass.class)
                        .addAnnotatedClass(ProductGroup.class)
                        .addAnnotatedClass(ProductSource.class)
                        .addAnnotatedClass(ProductStatus.class)
                        .addAnnotatedClass(Company.class)
                        .addAnnotatedClass(Account.class)
                        .addAnnotatedClass(AccountType.class);

                DBInfo info = DBInfo.parse(uri);

                if (info != null) {
                    conf.setProperty("hibernate.dialect", info.getDialect());
                    conf.setProperty("hibernate.connection.driver_class", info.getDriver());
                    conf.setProperty("hibernate.connection.url", info.getFullUri());
                    conf.setProperty("hibernate.connection.username", user);
                    conf.setProperty("hibernate.connection.password", pass);

                    String bs = Settings.getInstance().getString("BatchSize");
                    conf.setProperty("hibernate.jdbc.batch_size", bs);

                    StandardServiceRegistryBuilder srb = new StandardServiceRegistryBuilder().applySettings(conf.getProperties());

                    factory = conf.buildSessionFactory(srb.build());
                } else {
                    warning("database info not available for: " + uri);
                }

            } catch (Exception e) {
                warning("failed to create database", e);
            }
        }

        return factory;
    }

    /**
     * Create the SessionFactory using stored connection information.
     *
     * @return the SessionFactory or null on failure.
     */
    private SessionFactory createSessionFactory()
    {

        String username = Settings.getInstance().getConfig("username");
        String password = Settings.getInstance().getConfig("password");
        String database = Settings.getInstance().getConfig("uri");

        return createSessionFactory(database, username, password);
    }

    /**
     * Create a session using the current factory.
     *
     * @return a Session instance or null on failure.
     */
    @SuppressWarnings("WeakerAccess")
    public Session createSession()
    {
        if (factory != null)
            return factory.openSession();

        return null;
    }

    //region query

    private Predicate getPredicate(CriteriaBuilder cb, Expression<String> exp, Object val){
        if (val instanceof String) {
            String value = ((String) val).replaceAll("\\*", "%");
            return cb.like(cb.trim(exp), value);
        } else {
            return cb.equal(exp, val);
        }
    }

    public Product queryPrevious(String name){
        Transaction tx = null;
        Product result = null;

        try (Session session = createSession()){
            tx = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);
            Root<Product> root = criteria.from(Product.class);

            criteria.select(root)
                    .where(builder.lessThan(root.get("name"),name))
                    .orderBy(builder.desc(root.get("name")));

            result = session.createQuery(criteria)
                    .setMaxResults(1)
                    .getResultList()
                    .stream()
                    .findFirst()
                    .orElse(null);
        }
        catch(Exception e){
            if(tx != null){
                tx.rollback();
            }
        }

        return result;
    }

    public Product queryNext(String name){
        Transaction tx = null;
        Product result = null;

        try (Session session = createSession()){
            tx = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);
            Root<Product> root = criteria.from(Product.class);

            criteria.select(root)
                    .where(builder.greaterThan(root.get("name"),name))
                    .orderBy(builder.asc(root.get("name")));

            result = session.createQuery(criteria)
                    .setMaxResults(1)
                    .getResultList()
                    .stream()
                    .findFirst()
                    .orElse(null);
        }
        catch(Exception e){
            if(tx != null){
                tx.rollback();
            }
        }

        return result;
    }

    public <T extends BaseModel> T queryModel(Class<T> cls, int id){
        Transaction tx = null;
        T result = null;

        try (Session session = createSession()){
            tx = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<T> criteria = builder.createQuery(cls);
            Root<T> root = criteria.from(cls);

            criteria.select(root)
                    .where(builder.equal(root.get("id"),id));

            result = session.createQuery(criteria)
                    .setMaxResults(1)
                    .getResultList()
                    .stream()
                    .findFirst()
                    .orElse(null);
        }
        catch(Exception e){
            if(tx != null){
                tx.rollback();
            }
        }

        return result;
    }

    public <T extends BaseModel> List<T> query(Class<T> cls, List<Pair<String,Object>> patterns, Integer start, Integer count)
    {
        Transaction tx = null;
        List<T> results = new ArrayList<>();

        try (Session session = createSession()) {

            tx = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<T> criteria = builder.createQuery(cls);
            Root<T> baseRoot = criteria.from(cls);

            Join<Product,ProductClass> joinClass = null;
            Join<Product,ProductGroup> joinGroup = null;
            Join<Product,ProductSource> joinSource = null;
            Join<Product,ProductStatus> joinStatus = null;

            if(cls.equals(Product.class)){
                joinClass = baseRoot.join("productClass");
                joinGroup = baseRoot.join("productGroup");
                joinSource = baseRoot.join("productSource");
                joinStatus = baseRoot.join("productStatus");
            }

            if(patterns.size() > 0){
                String key;
                Object val;

                Predicate[] predicates = new Predicate[patterns.size()];

                for(int i = 0; i < patterns.size(); ++i){

                    Pair<String,Object> pattern = patterns.get(i);

                    key = pattern.getKey();
                    val = pattern.getValue();

                    if(val == null)
                        continue;

                    if(key.contains(".") && cls.equals(Product.class)){
                        String[] path = key.split("\\.");
                        key = path[path.length-1];

                        switch(path[0]){
                            case "productClass":
                                predicates[i] = getPredicate(builder,joinClass.get(key),val); break;
                            case "productGroup":
                                predicates[i] = getPredicate(builder,joinGroup.get(key),val); break;
                            case "productSource":
                                predicates[i] = getPredicate(builder,joinSource.get(key),val); break;
                            case "productStatus":
                                predicates[i] = getPredicate(builder,joinStatus.get(key),val); break;
                        }
                    }
                    else {
                        if (val instanceof String) {
                            String value = ((String) val).replaceAll("\\*", "%");
                            predicates[i] = builder.like(builder.trim(baseRoot.get(key)), value);
                        } else {
                            predicates[i] = builder.equal(baseRoot.get(key), val);
                        }
                    }
                }

                criteria.orderBy(builder.asc(baseRoot.get("name")));
                criteria.select(baseRoot).where(builder.or(predicates));

            }

            Query<T> query = session.createQuery(criteria);

            if (start != null)
                query.setFirstResult(start);

            if (count != null){
                query.setMaxResults(count);
            }

            results = query.getResultList();

        } catch (Exception e) {
            if (tx != null){
                try {
                    tx.rollback();
                }
                catch(Exception r){
                    warning("could not rollback transaction");
                }
            }
            warning("exception during query",e);
        }

        return results;
    }

    public final <T extends BaseModel> List<T> query(Class<T> cls, List<Pair<String,Object>> patterns)
    {
        return query(cls,patterns,null,null);
    }

    public final <T extends BaseModel> List<T> query(Class<T> cls, Integer start, Integer count, List<Pair<String,Object>> patterns)
    {
        return query(cls,patterns,start,count);
    }

    @SafeVarargs
    public final <T extends BaseModel> List<T> query(Class<T> cls, Pair<String,Object>... patterns)
    {
        return query(cls,List.of(patterns));
    }

    @SafeVarargs
    public final <T extends BaseModel> List<T> query(Class<T> cls, Integer start, Integer count, Pair<String,Object>... patterns)
    {
        return query(cls,start,count,List.of(patterns));
    }

    //endregion query

    //region save

    public void save(List<BaseModel> models)
    {
        Transaction tx = null;

        try (Session session = createSession()) {

            tx = session.beginTransaction();
            for (BaseModel model : models) {
                session.saveOrUpdate(model);
            }

            tx.commit();
        } catch (Exception e) {

            if (tx != null)
                tx.rollback();

            throw e;
        }
    }

    public void save(BaseModel... models)
    {
        save(List.of(models));
    }

    //endregion save

    //region delete

    public void delete(BaseModel model)
    {
        Transaction tx = null;

        try (Session session = createSession()) {

            tx = session.beginTransaction();
            session.delete(model);

            tx.commit();
        } catch (Exception e) {

            if (tx != null)
                tx.rollback();

            throw e;
        }
    }

    //endregion delete

    /**
     * Try to connect to a database using new credentials to verify that
     * they are valid.
     *
     * @param uri  the uri to connect to.
     * @param user the username to connect with.
     * @param pass the password to connect with.
     * @return true if the connection succeeded.
     */
    public boolean testCredentials(String uri, String user, String pass)
    {
        return (createSessionFactory(uri, user, pass) != null);
    }

}
