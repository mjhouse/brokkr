package core.controllers;

import core.communication.events.ControllerEvent;
import core.communication.flags.EventType;
import core.communication.interfaces.Loggable;
import core.communication.interfaces.Subscriber;
import core.models.collections.WidgetCollection;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Menu;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A common base class for all core.controllers.
 */
public class BaseController implements Loggable, Subscriber {

    private final Node root;

    private static ArrayList<BaseController> workspaces;

    static {
        workspaces = new ArrayList<>();
    }

    public BaseController(String path){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            loader.setController(this);

            loader.load();

            root = loader.getRoot();
            getEvent(EventType.ALL);

            workspaces.add(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<Menu> getMenuCollection()
    {
        return new ArrayList<>();
    }

    public WidgetCollection getWidgetCollection()
    {
        return new WidgetCollection();
    }

    public static ArrayList<BaseController> getWorkspaces(){
        return workspaces;
    }

    public Node getRoot()
    {
        return root;
    }

    @Override
    public void onEvent(ControllerEvent event)
    {
    }

}
