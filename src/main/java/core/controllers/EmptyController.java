package core.controllers;

public class EmptyController extends BaseController {

    public EmptyController(){
        super("/views/workspaces/empty_workspace.fxml");
    }

    /**
     * Initial setup for the workspace.
     */
    public void initialize()
    {
    }
}
