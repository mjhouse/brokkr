package core.controllers;

import core.communication.flags.EventType;
import financial.FinancialController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Separator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import products.ProductsController;
import settings.SettingsController;
import settings.models.Settings;

import java.util.ArrayList;
import java.util.Map;

import static java.util.Map.entry;

/**
 * The controller for the Main window.
 */
public class MainController extends BaseController {

    /**
     * The currentWorkspace workspace flag.
     */
    private static BaseController currentWorkspace;
    /**
     * The menu for the currentWorkspace workspace.
     */
    @FXML
    public MenuBar workspaceMenu;
    /**
     * The box that contains the currentWorkspace workspace controls.
     */
    @FXML
    public HBox workspaceWidgets;
    public Separator workspaceSep;
    /**
     * The currentWorkspace workspace.
     */
    @FXML
    private AnchorPane workspace;
    /**
     * A map of workspace enums and loaded FXML layouts.
     */
    private Map<String,BaseController> workspaces;

    public MainController(){
        super("/views/mainwindow.fxml");
    }

    /**
     * Initialize the main window.
     */
    @FXML
    protected void initialize()
    {
        workspaces = Map.ofEntries(
                entry("Empty", new EmptyController()),
                entry("Financial", new FinancialController()),
                entry("Products", new ProductsController()),
                entry("Settings", new SettingsController())
        );

        changeWorkspace(Settings.getInstance().getString("StartWorkspace"));
    }

    /**
     * Handler for the quit action.
     *
     * @param event the event object.
     */
    @FXML
    @SuppressWarnings("unused")
    public void handleQuit(ActionEvent event)
    {
        System.exit(0);
    }

    /**
     * Change to the Settings workspace.
     *
     * @param event the event object.
     */
    @FXML
    @SuppressWarnings("unused")
    public void workspaceSettings(ActionEvent event)
    {
        changeWorkspace("Settings");
    }

    /**
     * Change to the Empty workspace.
     *
     * @param event the event object.
     */
    @FXML
    @SuppressWarnings("unused")
    public void workspaceNone(ActionEvent event)
    {
        changeWorkspace("Empty");
    }

    /**
     * Change to the Products workspace.
     *
     * @param event the event object.
     */
    @FXML
    @SuppressWarnings("unused")
    public void workspaceProducts(ActionEvent event)
    {
        changeWorkspace("Products");
    }

    /**
     * Change to the Financial workspace.
     *
     * @param event the event object.
     */
    @FXML
    @SuppressWarnings("unused")
    public void workspaceFinancial(ActionEvent event)
    {
        changeWorkspace("Financial");
    }
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Change to a workspace given its title.
     *
     * @param name the name of the workspace.
     */
    private void changeWorkspace(String name)
    {
        currentWorkspace = workspaces.get(name);
        workspace.getChildren().clear();
        workspace.getChildren().add(currentWorkspace.getRoot());

        ArrayList<Menu> menus = currentWorkspace.getMenuCollection();
        ArrayList<Node> widgets = currentWorkspace.getWidgetCollection();

        workspaceMenu.getMenus().setAll(menus);
        workspaceWidgets.getChildren().setAll(widgets);

        workspaceSep.setVisible(!(menus.isEmpty() || widgets.isEmpty()));

        info("changed workspace to: " + name);
        sendEvent(EventType.LOAD);
    }

}
