package core.models;

import core.communication.interfaces.Loggable;
import core.database.DatabaseManager;
import core.models.properties.BaseProperty;
import core.models.properties.IntegerProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A base model that provides database fields common to all database-aware business objects.
 */
@SuppressWarnings("unused")
@MappedSuperclass
@Access(value = AccessType.PROPERTY)
public abstract class BaseModel implements Loggable {

    private List<BaseProperty> properties;

    protected final IntegerProperty id;

    /**
     * Empty constructor of the base model.
     */
    public BaseModel()
    {
        properties = new ArrayList<>();
        id = new IntegerProperty(this,"id", 0);
    }

    public void addProperty(BaseProperty property){
        properties.add(property);
    }

    public List<BaseProperty> allProperties()
    {
        return this.properties;
    }

    public BaseProperty propertyValue(String name)
    {
        for(BaseProperty property : properties){
            if(name.equals(property.name()))
                return property;
        }
        return null;
    }

    public List<String> propertyNames(){
        return properties.stream()
                .map(BaseProperty::name)
                .collect(Collectors.toList());
    }

    public int propertyCount(){
        return properties.size();
    }

    //region properties

    //region id

    @Id
    @SequenceGenerator(name = "default_gen", sequenceName = "default_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId()
    {
        return id.get();
    }

    public IntegerProperty idProperty()
    {
        return id;
    }

    public void setId(Integer statusId)
    {
        id.set(statusId);
    }

    //endregion id

    //endregion properties

    public abstract void cloneModel(BaseModel other);

    public abstract void resetModel();

    public abstract boolean inDatabase();

    public void delete(){
        DatabaseManager.getInstance().delete(this);
    }

    protected String trim(String value){
        if(value != null)
            value = value.trim();
        return value;
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof BaseModel){
            return getId().equals( ((BaseModel)other).getId());
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        return getId().hashCode();
    }

    public String stringValue(){
        return "";
    }
}
