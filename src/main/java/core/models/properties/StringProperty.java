package core.models.properties;

import core.models.BaseModel;

public class StringProperty extends BaseProperty<String> {

    public StringProperty(String value)
    {
        super(value);
    }

    public StringProperty(String name, String value)
    {
        super(name, value);
    }

    public StringProperty(BaseModel parent, String name, String value)
    {
        super(parent, name, value);
    }

    public void set(String value){
        super.set(value);
    }
}
