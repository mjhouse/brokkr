package core.models.properties;

import core.models.BaseModel;
import javafx.util.StringConverter;

public class BooleanProperty extends BaseProperty<Boolean> {

    public BooleanProperty(Boolean value)
    {
        super(value);
    }

    public BooleanProperty(String name, Boolean value)
    {
        super(name, value);
    }

    public BooleanProperty(BaseModel parent, String name, Boolean value)
    {
        super(parent, name, value);
    }

    public void set(Boolean value){
        super.set(value);
    }
}
