package core.models.properties;

import core.models.BaseModel;

public class ModelProperty<T extends BaseModel> extends BaseProperty<T> {

    private String name;

    public ModelProperty(T value)
    {
        super(value);
    }

    public ModelProperty(String name, T value)
    {
        super(name, value);
    }

    public ModelProperty(BaseModel parent, String name, T value)
    {
        super(parent, name, value);
    }

    public void set(T value){
        BaseModel current = super.get();
        if(current == null){
            super.set(value);
        }
        else{
            current.cloneModel(value);
        }
    }

    @Override
    public void reset(){
        BaseModel model = get();
        model.resetModel();
    }

    @Override
    public String getStringValue()
    {
        BaseModel model = get();
        return model.stringValue();
    }

}
