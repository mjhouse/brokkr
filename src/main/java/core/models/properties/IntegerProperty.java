package core.models.properties;

import core.models.BaseModel;

public class IntegerProperty extends BaseProperty<Integer> {
    public IntegerProperty(Integer value)
    {
        super(value);
    }

    public IntegerProperty(String name, Integer value)
    {
        super(name, value);
    }

    public IntegerProperty(BaseModel parent, String name, Integer value)
    {
        super(parent, name, value);
    }

    public void set(Integer value){
        super.set(value);
    }
}
