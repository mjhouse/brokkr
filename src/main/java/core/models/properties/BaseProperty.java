package core.models.properties;

import core.models.BaseModel;
import javafx.beans.property.SimpleObjectProperty;

import java.lang.ref.WeakReference;

public class BaseProperty<T> extends SimpleObjectProperty<T> {

    private WeakReference<BaseModel> parent;

    private String name;

    private T initial;

    public BaseProperty(T value){
        internalConstructor(null,null,value);
    }

    public BaseProperty(String name, T value){
        internalConstructor(null,name,value);
    }

    public BaseProperty(BaseModel parent, String name, T value){
        internalConstructor(parent,name,value);
    }

    private void internalConstructor(BaseModel parent, String name, T value){
        this.name = name;
        this.parent = new WeakReference<>(parent);

        if(parent != null){
            parent.addProperty(this);
        }

        initial = value;
        set(value);
    }

    public void reset(){
        setValue(initial);
    }

    public String name(){
        return this.name;
    }

    public Object value(){
        return this.get();
    }

    public BaseModel parent(){
        return this.parent.get();
    }

    public boolean isInitial(){
        Object value = getValue();
        return     (initial == null && value == null)
                || (initial != null && initial.equals(value));
    }

    public String getStringValue()
    {
        return get().toString();
    }
}
