package core.models.properties;

import core.models.BaseModel;

public class DoubleProperty extends BaseProperty<Double> {

    public DoubleProperty(Double value)
    {
        super(value);
    }

    public DoubleProperty(String name, Double value)
    {
        super(name, value);
    }

    public DoubleProperty(BaseModel parent, String name, Double value)
    {
        super(parent, name, value);
    }

    public void set(Double value){
        super.set(value);
    }

}
