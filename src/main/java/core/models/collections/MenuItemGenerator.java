package core.models.collections;

import javafx.scene.control.MenuItem;
import java.util.List;

public interface MenuItemGenerator {

    List<MenuItem> getList();

}
