package core.models.collections;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;

import java.util.ArrayList;

public class WidgetCollection extends ArrayList<Node> {

    public WidgetCollection withButton(String name, EventHandler<ActionEvent> handler){

        Button btn = new Button();

        btn.setText(name);
        btn.setOnAction(handler);

        add(btn);
        return this;
    }

    public WidgetCollection with(Node node){
        add(node);
        return this;
    }

}
