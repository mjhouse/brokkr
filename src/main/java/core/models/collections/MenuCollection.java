package core.models.collections;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

import java.util.ArrayList;

public class MenuCollection extends ArrayList<Menu> {

    private Menu working = null;

    public MenuCollection withMenu(String name){
        working = new Menu();
        working.setText(name);

        add(working);
        return this;
    }

    public MenuCollection withItem(String name, EventHandler<ActionEvent> onClick){
        MenuItem item = new MenuItem(name);
        item.setOnAction(onClick);

        working.getItems().add(item);
        return this;
    }

    public MenuCollection withItems(MenuItemGenerator generator){
        for(MenuItem item : generator.getList()){
            working.getItems().add(item);
        }
        return this;
    }


}
