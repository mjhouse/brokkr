package products.components;

import core.communication.interfaces.Loggable;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import products.models.Trend;

import java.io.IOException;
import java.net.URL;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class AutoTrendChart extends VBox implements Initializable, Loggable {

    //region Static Fields

    private final static String DATA = "DATA";

    private final static double MINUTE = 60;
    private final static double HOUR = MINUTE * 60;
    private final static double DAY = HOUR * 24;
    private final static double WEEK = DAY * 7;
    private final static double MONTH = WEEK * 4;
    private final static double YEAR = MONTH * 12;

    //endregion Static Fields

    //region Private Fields

    private Trend trend;

    //endregion Private Fields

    //region FXML Fields

    @FXML
    private DatePicker start;

    @FXML
    private DatePicker end;

    @FXML
    private AreaChart<Number, Number> chart;

    @FXML
    private Label xLabel;

    @FXML
    private Label yLabel;

    //endregion FXML Fields

    //region Private Classes

    public AutoTrendChart()
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/components/AutoTrendChart.fxml"));

        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {

        NumberAxis xAxis = (NumberAxis) chart.getXAxis();

        xAxis.setAutoRanging(false);
        xAxis.setTickLabelFormatter(new LocalDateTimeConverter());

        chart.addEventHandler(MouseEvent.MOUSE_MOVED, this::onMouseMoved);

        start.setDayCellFactory(StartDateTimeCellFactory::new);
        end.setDayCellFactory(EndDateTimeCellFactory::new);

        start.setEditable(false);
        end.setEditable(false);

        start.valueProperty().addListener(this::onDateChanged);
        end.valueProperty().addListener(this::onDateChanged);

        start.setValue(LocalDate.now().minusYears(1));
        end.setValue(LocalDate.now());
    }

    private double getInterval()
    {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        double upper = xAxis.getUpperBound();
        double lower = xAxis.getLowerBound();

        double range = upper - lower;

        if (range <= DAY) {
            return HOUR;
        } else if (range <= WEEK) {
            return DAY;
        } else if (range <= MONTH) {
            return WEEK;
        } else if (range <= YEAR) {
            return MONTH;
        } else {
            return YEAR;
        }
    }

    //endregion Private Classes

    private void onMouseMoved(MouseEvent e)
    {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        NumberAxis yAxis = (NumberAxis) chart.getYAxis();
        Point2D point = new Point2D(e.getSceneX(), e.getSceneY());

        double xPos = xAxis.sceneToLocal(new Point2D(point.getX(), 0)).getX();
        double yPos = yAxis.sceneToLocal(new Point2D(0, point.getY())).getY();

        Number xVal = xAxis.getValueForDisplay(xPos);
        Number yVal = yAxis.getValueForDisplay(yPos);

        xLabel.setText(xAxis.getTickLabelFormatter().toString(xVal));
        yLabel.setText(String.format("%.2f", yVal.doubleValue()));
    }

    private void onDateChanged(Observable obs)
    {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();

        LocalDate startValue = start.getValue();
        LocalDate endValue = end.getValue();

        if (startValue != null && endValue != null) {
            LocalDateTime startDate = start.getValue().atStartOfDay();
            LocalDateTime endDate = end.getValue().atStartOfDay();

            if (!startDate.isAfter(endDate)) {
                ZoneOffset offset = OffsetDateTime.now(ZoneId.systemDefault()).getOffset();

                xAxis.setUpperBound(endDate.toEpochSecond(offset));
                xAxis.setLowerBound(startDate.toEpochSecond(offset));

                xAxis.setTickUnit(getInterval());
            } else {
                start.setValue(endDate.toLocalDate());
            }
        }
    }

    //region Private

    /**
     * Gets the data displayed in the chart.
     *
     * @return data as a series.
     */
    public XYChart.Series<LocalDateTime, Number> getData()
    {
        // the result should be null unless there is data in the chart
        XYChart.Series<LocalDateTime, Number> result = null;

        XYChart.Series<Number, Number> data = chart.getData().stream().filter(s -> DATA.equals(s.getName())).findFirst().orElse(null);

        if (data != null) {

            // return null if no data
            result = new XYChart.Series<>();

            for (XYChart.Data<Number, Number> point : data.getData()) {

                // get the epoch for this point
                long epoch = point.getXValue().longValue();

                // convert to LocalDateTime
                LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.systemDefault());

                // append to the result series
                result.getData().add(new XYChart.Data<>(date, point.getYValue()));
            }
        }

        return result;
    }

    //endregion Private

    //region Handlers

    /**
     * Set the data to display in the chart.
     *
     * @param series data as a series.
     */
    public void setData(XYChart.Series<LocalDateTime, Number> series)
    {
        XYChart.Series<Number, Number> result = new XYChart.Series<>();
        ZoneId tz = ZoneId.systemDefault();

        for (XYChart.Data<LocalDateTime, Number> point : series.getData()) {

            LocalDateTime date = point.getXValue();
            ZoneOffset offset = tz.getRules().getOffset(date);

            // convert to LocalDateTime
            Number epoch = date.toEpochSecond(offset);
            Number value = point.getYValue();

            // append to the result series
            result.getData().add(new XYChart.Data<Number, Number>(epoch, value));
        }

        result.setName(DATA);
        chart.getData().add(result);
    }

    /**
     * Get the current trend for the chart.
     *
     * @return the current trend.
     */
    public Trend getTrend()
    {
        return trend;
    }

    //endregion Handlers

    //region Public

    /**
     * Set the current trend for the chart.
     *
     * @param line the trend line to use.
     */
    public void setTrend(Trend line)
    {
        line.bind(chart);
        trend = line;
    }

    /**
     * Get the formatting pattern for values on the
     * x axis.
     *
     * @return the datetime format being used
     */
    public String getFormat()
    {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        LocalDateTimeConverter converter = (LocalDateTimeConverter) xAxis.getTickLabelFormatter();
        return converter.getFormat();
    }

    /**
     * Set the format for values on the x axis.
     *
     * @param pattern the datetime format to use
     */
    public void setFormat(String pattern)
    {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        LocalDateTimeConverter converter = (LocalDateTimeConverter) xAxis.getTickLabelFormatter();
        converter.setFormat(pattern);
    }

    private class LocalDateTimeConverter extends StringConverter<Number> {

        private final ZoneId tz = ZoneId.systemDefault();
        private DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        String getFormat()
        {
            return this.format.toString();
        }

        void setFormat(String pattern)
        {
            this.format = DateTimeFormatter.ofPattern(pattern);
        }

        @Override
        public String toString(Number object)
        {
            LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochSecond(object.longValue()), tz);
            return date.format(format);
        }

        @Override
        public Number fromString(String string)
        {
            LocalDateTime date = LocalDateTime.parse(string, format);
            ZoneOffset offset = tz.getRules().getOffset(LocalDateTime.now());

            // convert to LocalDateTime
            return date.toEpochSecond(offset);
        }
    }

    private class StartDateTimeCellFactory extends DateCell {

        private final DatePicker picker;

        StartDateTimeCellFactory(DatePicker p)
        {
            picker = p;
        }

        @Override
        public void updateItem(LocalDate date, boolean empty)
        {
            super.updateItem(date, empty);
            if (date != null && end != null) {
                setDisable(empty || date.isAfter(LocalDate.now()) || date.isAfter(end.getValue()));
            }
        }

    }

    private class EndDateTimeCellFactory extends DateCell {

        private final DatePicker picker;

        EndDateTimeCellFactory(DatePicker p)
        {
            picker = p;
        }

        @Override
        public void updateItem(LocalDate date, boolean empty)
        {
            super.updateItem(date, empty);
            setDisable(empty || date.isAfter(LocalDate.now()));
        }

    }

    //endregion Public


    //    //region Private Methods
    //
    //    private void setXPositionLabel(double x) {
    //        LocalDate date = LocalDate.ofEpochDay((long)x);
    //        xLabel.setText(date.format(format));
    //    }
    //
    //    private void setYPositionLabel(double y) {
    //        yLabel.setText(String.format("%.2f",y));
    //    }
    //
    //    private XYChart.Series<Number, Number> query(){
    //        return chart.query()
    //                .stream()
    //                .filter(s -> DATA.equals(s.name()))
    //                .findFirst()
    //                .orElse(null);
    //    }
    //
    //    private double getTickUnit(long start, long end) {
    //        long delta = end - start;
    //        if(delta < WEEK*2){
    //            return DAY;
    //        }
    //        else if(delta < MONTH*2){
    //            return WEEK;
    //        }
    //        else if(delta < YEAR*2){
    //            return MONTH;
    //        }
    //        else {
    //            return YEAR;
    //        }
    //    }
    //
    //    private void setChartBounds(LocalDate start, LocalDate end) {
    //        long epochStart = start.toEpochDay();
    //        long epochEnd = end.toEpochDay();
    //
    //        usageXAxis.setTickUnit(getTickUnit(epochStart,epochEnd));
    //        usageXAxis.setUpperBound(epochEnd);
    //        usageXAxis.setLowerBound(epochStart);
    //
    //        setTrendLines(epochStart,epochEnd);
    //    }
    //
    //    private void setTrendLines(long start, long end){
    //        for(Map.Entry<String,FitType> info: trends.entrySet()){
    //            switch(info.getValue()){
    //                case LINEAR:
    //                    createLinearTrend(start,end,info.getKey());
    //                    break;
    //                default:
    //                    break;
    //            }
    //        }
    //    }
    //
    //    private void createLinearTrend(long x_min, long x_max, String name) {
    //        // DATA is reserved for the dataset
    //        if(DATA.equals(name)) return;
    //
    //        // get the usage chart from the graph
    //        XYChart.Series<Number, Number> usage = query();
    //
    //        if (usage != null) {
    //
    //            ArrayList<double[]> results = new ArrayList<>();
    //
    //            for (XYChart.Data<Number, Number> point : usage.query()) {
    //                double x = point.getXValue().doubleValue();
    //                double y = point.getYValue().doubleValue();
    //                if (x > x_min && x < x_max) {
    //                    results.add(new double[]{x, y});
    //                }
    //            }
    //
    //            double[][] array = new double[results.size()][2];
    //            array = results.toArray(array);
    //
    //            SimpleRegression linear = getLinearRegression();
    //            linear.addData(array);
    //
    //            XYChart.Series<Number, Number> trend = new XYChart.Series<>();
    //
    //            for (double[] point : results) {
    //                double x = point[0];
    //                double m = linear.getSlope();
    //                double b = linear.getIntercept();
    //
    //                double ny = m * x + b;
    //
    //                if(ny > 0)
    //                    trend.query().add(new XYChart.Data<>(x, ny));
    //            }
    //
    //            chart.query().removeIf(d -> name.equals(d.name()));
    //            trend.setName(name);
    //
    //            chart.query().add(trend);
    //
    //            trend.getNode().getStyleClass().add(TREND_CLASS);
    //            applyCss();
    //
    //            Optional max = trend.query().stream().max(Comparator.comparing(d -> d.getXValue().intValue()));
    //            Optional min = trend.query().stream().min(Comparator.comparing(d -> d.getXValue().intValue()));
    //
    //            if(max.isPresent() && min.isPresent()){
    //
    //                x_max = ((Number)((XYChart.Data)max.get()).getXValue()).longValue();
    //                x_min = ((Number)((XYChart.Data)min.get()).getXValue()).longValue();
    //
    //                createPrediction(x_min,x_max);
    //            }
    //
    //        }
    //    }
    //
    //    private SimpleRegression getLinearRegression(){
    //
    //        double x_max = usageXAxis.getUpperBound();
    //        double x_min = usageXAxis.getLowerBound();
    //
    //        XYChart.Series<Number, Number> usage = query();
    //        ArrayList<double[]> results = new ArrayList<>();
    //
    //        for (XYChart.Data<Number, Number> point : usage.query()) {
    //            double x = point.getXValue().doubleValue();
    //            double y = point.getYValue().doubleValue();
    //            if (x > x_min && x < x_max) {
    //                results.add(new double[]{x, y});
    //            }
    //        }
    //
    //        double[][] array = new double[results.size()][2];
    //        array = results.toArray(array);
    //
    //        SimpleRegression linear = new SimpleRegression(true);
    //        linear.addData(array);
    //
    //        return linear;
    //    }
    //
    //    private void createPrediction(long start, long stop, long step, String name ){
    //        XYChart.Series<Number, Number> trend = new XYChart.Series<>();
    //        SimpleRegression reg = getLinearRegression();
    //
    //        if(step > 0){
    //            for(long i = start; i < stop; i += step){
    //                double prediction = reg.predict(i);
    //                if(prediction > 0){
    //                    trend.query().add(new XYChart.Data<>(i,prediction));
    //                }
    //            }
    //            chart.query().removeIf(d -> name.equals(d.name()));
    //            trend.setName(name);
    //
    //            chart.query().add(trend);
    //
    //            trend.getNode().getStyleClass().add(PREDICTION_CLASS);
    //            applyCss();
    //
    //        }
    //    }
    //
    //    private void createPrediction(long x_min, long x_max){
    //        long x_start = (long)usageXAxis.getLowerBound();
    //        long x_end   = (long)usageXAxis.getUpperBound();
    //
    //        createPrediction(x_max,x_end,1,TRAILING);
    //        createPrediction(x_start,x_min,1,LEADING);
    //    }
    //
    //    //endregion Private Methods
    //
    //    //region Public Methods
    //
    //    public void setTrend(String name, FitType fit){
    //        if(name == null || DATA.equals(name)) return;
    //        trends.put(name,fit);
    //
    //        LocalDate start = start.getValue();
    //        LocalDate end = end.getValue();
    //        setChartBounds(start,end);
    //    }
    //
    //    public void setData(Map<LocalDateTime,Double> data){
    //        XYChart.Series<Number, Number> chart = new XYChart.Series<>();
    //        ZoneId tz = ZoneId.systemDefault();
    //
    //        double max = 100;
    //
    //        for(Map.Entry<LocalDateTime,Double> point : data.entrySet()){
    //            double usage = point.getValue();
    //            double epoch = point.getKey().atZone(tz).toLocalDate().toEpochDay();
    //
    //            usage = usage > 0 ? usage : 0;
    //            epoch = epoch > 0 ? epoch : 0;
    //
    //            chart.query().add(new XYChart.Data<>(epoch, usage));
    //            if(usage > max) max = usage;
    //        }
    //
    //        chart.query().removeIf(d -> DATA.equals(d.name()));
    //        chart.setName(DATA);
    //
    //        chart.query().add(chart);
    //        usageYAxis.setUpperBound(max);
    //
    //        chart.getNode().getStyleClass().add(DATA_CLASS);
    //        applyCss();
    //
    //    }
    //
    //    public void clearData(){
    //        chart.query().resetValue();;
    //        trends.resetValue();
    //    }
    //
    //    //endregion Public Methods
    //
    //    //region Handlers
    //
    //    private void onDateChanged(ActionEvent actionEvent) {
    //        LocalDate start = start.getValue();
    //        LocalDate end = end.getValue();
    //        LocalDate today = LocalDate.now(ZoneId.systemDefault());
    //        if(start.isAfter(end)) start.setValue(end);
    //
    //        setChartBounds( start, end );
    //    }
    //
    //
    //
    //    //endregion Handlers

}
