package products.components;

import core.communication.interfaces.Loggable;
import core.components.DoubleField;
import core.components.Dialog;
import core.database.DatabaseManager;
import core.helpers.DateTimeHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;
import products.models.*;
import settings.models.Settings;

import java.net.URL;
import java.util.ResourceBundle;

@SuppressWarnings("FieldCanBeLocal")
public class CreateProductWindow extends Dialog implements Loggable {

    private Product product;

    //    @FXML private IntegerField productId;

    @FXML private TextField name;

    @FXML private CheckBox current;

    @FXML private Label author;

    @FXML private TextArea description;

    //    @FXML private TextField units;

    @FXML private DoubleField weight;

    @FXML private CheckBox backflush;

    @FXML
    private ComboBox<Product> revision;
    private ObservableList<Product> revisionList;

    @FXML
    private ComboBox<ProductClass> classes;
    private ObservableList<ProductClass> classList;

    @FXML
    private ComboBox<ProductGroup> group;
    private ObservableList<ProductGroup> groupList;

    @FXML
    private ComboBox<ProductSource> source;
    private ObservableList<ProductSource> sourceList;

    @FXML
    private ComboBox<ProductStatus> status;
    private ObservableList<ProductStatus> statusList;

    public CreateProductWindow(){
        super("Create Product", "/views/components/CreateProductWindow.fxml");

        addButton("Clear",ButtonData.RIGHT,this::onClear);
        addButton("Create",ButtonData.RIGHT,this::onCreate);

        setBarPadding(0,10,10,10);
    }

    private void onClear(ActionEvent event)
    {
        setEmptyState();
    }

    private void onCreate(ActionEvent actionEvent)
    {
        if(preProcess()){
            DatabaseManager.getInstance().save(product);
            postProcess();
            close();
        }
    }

    //region processing

    private void postProcess()
    {
        if(product.getName().isEmpty()){
            product.nameProperty().unbind();
            product.setName("Product_" + product.getId());
            DatabaseManager.getInstance().save(product);
        }
    }

    private boolean preProcess()
    {
        // set the created timestamp to now
        product.setCreated(DateTimeHelper.getTimeStamp());


        if(product.isCurrent()){
            Product last = product.getAllRevisions()
                    .stream()
                    .filter(p -> {
                        return !p.equals(product) && p.isCurrent();
                    })
                    .findFirst()
                    .orElse(null);

            if(last != null){
                last.setCurrent(false);
                DatabaseManager.getInstance().save(last);
            }
        }

        return true;
    }

    //endregion processing

    @Override
    public void initialize(URL location, ResourceBundle resources){

        product = new Product();

        bindToProduct(product);
        setEmptyState();
    }

    private void setEmptyState()
    {
        name.setText("");
        current.setSelected(false);
        description.setText("");
        weight.setValue(0.0);
        backflush.setSelected(false);

        classes.getSelectionModel().clearAndSelect(0);
        group.getSelectionModel().clearAndSelect(0);
        source.getSelectionModel().clearAndSelect(0);
        status.getSelectionModel().clearAndSelect(0);
        revision.getSelectionModel().clearAndSelect(0);

        // user is automatically filled from the configured username
        String user = Settings.getInstance().getConfig("username");
        author.setText(user);
        // units;
    }

    //region binding

    private void bindToProduct(Product product)
    {
        // fetch and fill all combo boxes
        classList  = bindClass(classes);
        statusList = bindStatus(status);
        groupList  = bindGroup(group);
        sourceList = bindSource(source);
        revisionList = bindRevisions(revision);

        //revisionList = bindRevision(revision)

        // clone product associated classes when combos change
        classes.valueProperty().addListener((obs,o,n) -> product.getProductClass().cloneModel(n));
        status.valueProperty().addListener((obs,o,n) -> product.getProductStatus().cloneModel(n));
        group.valueProperty().addListener((obs,o,n) -> product.getProductGroup().cloneModel(n));
        source.valueProperty().addListener((obs,o,n) -> product.getProductSource().cloneModel(n));
        revision.valueProperty().addListener((obs,o,n) -> product.setProductId(n.getProductId()));

        product.getProductClass().cloneModel(classes.getValue());
        product.getProductStatus().cloneModel(status.getValue());
        product.getProductGroup().cloneModel(group.getValue());
        product.getProductSource().cloneModel(source.getValue());
        product.setProductId(revision.getValue().getProductId());

        //revision.valueProperty().addListener((obs,o,n) -> product.setProductId(n.getProductId()));

        // bind product properties to ui properties
        product.nameProperty().bind(name.textProperty());
        product.currentProperty().bind(current.selectedProperty());
        product.authorProperty().bind(author.textProperty());
        product.descriptionProperty().bind(description.textProperty());
        product.weightProperty().bind(weight.valueProperty());
        product.backflushProperty().bind(backflush.selectedProperty());

        //product.unitsProperty().bind(units.selectedProperty());
        //created is set on save
    }

    private ObservableList<Product> bindRevisions(ComboBox<Product> revision)
    {
        ObservableList<Product> list = FXCollections.observableArrayList();
        list.setAll(Product.getAll());

        revision.setItems(list);
        revision.setValue(list.get(0));

        Product.STRING_CONVERTER.setClasses(list);
        revision.setConverter(Product.STRING_CONVERTER);

        return list;
    }

    private ObservableList<ProductSource> bindSource(ComboBox<ProductSource> source)
    {
        ObservableList<ProductSource> list = FXCollections.observableArrayList();
        list.setAll(ProductSource.getAll());

        source.setItems(list);
        source.setValue(list.get(0));

        ProductSource.STRING_CONVERTER.setClasses(list);
        source.setConverter(ProductSource.STRING_CONVERTER);

        return list;
    }

    private ObservableList<ProductGroup> bindGroup(ComboBox<ProductGroup> group)
    {
        ObservableList<ProductGroup> list = FXCollections.observableArrayList();
        list.setAll(ProductGroup.getAll());

        group.setItems(list);
        group.setValue(list.get(0));

        ProductGroup.STRING_CONVERTER.setClasses(list);
        group.setConverter(ProductGroup.STRING_CONVERTER);

        return list;
    }

    private ObservableList<ProductStatus> bindStatus(ComboBox<ProductStatus> status)
    {
        ObservableList<ProductStatus> list = FXCollections.observableArrayList();
        list.setAll(ProductStatus.getAll());

        status.setItems(list);
        status.setValue(list.get(2));

        ProductStatus.STRING_CONVERTER.setClasses(list);
        status.setConverter(ProductStatus.STRING_CONVERTER);

        return list;
    }

    private ObservableList<ProductClass> bindClass(ComboBox<ProductClass> classes)
    {
        ObservableList<ProductClass> list = FXCollections.observableArrayList();
        list.setAll(ProductClass.getAll());

        classes.setItems(list);
        classes.setValue(list.get(0));

        ProductClass.STRING_CONVERTER.setClasses(list);
        classes.setConverter(ProductClass.STRING_CONVERTER);

        return list;
    }

    //endregion binding

}
