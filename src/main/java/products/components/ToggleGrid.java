package products.components;

import core.communication.interfaces.Loggable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ToggleGrid extends GridPane implements Initializable, Loggable {

    private ToggleGroup group;

    private List<RadioButton> buttons;

    private ContentDisplay display;

    private StringProperty toggled;

    private boolean mnemonicParsing;

    private int columns;

    private int rows;

    public ToggleGrid()
    {
        columns = 1;
        rows = -1;
        group = new ToggleGroup();
        buttons = new ArrayList<>();
        display = ContentDisplay.LEFT;
        mnemonicParsing = false;
        toggled = new SimpleStringProperty("");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/components/ToggleGrid.fxml"));

        loader.setRoot(this);
        loader.setController(this);

        toggled.addListener((e) -> {
            String label = getToggled();
            for (RadioButton btn : buttons) {
                if (btn.getText().equals(label)) {
                    btn.setSelected(true);
                }
                else {
                    btn.setSelected(false);
                }
            }
        });

        group.selectedToggleProperty().addListener((e) -> {
            RadioButton btn = (RadioButton) group.getSelectedToggle();
            if (btn != null) {
                String label = btn.getText();
                setToggled(label);
            }
        });

        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
    }

    public ToggleGroup getGroup()
    {
        return group;
    }

    public void setGroup(ToggleGroup group)
    {
        this.group = group;
    }

    public List<RadioButton> getButtons()
    {
        return buttons;
    }

    public void setButtons(List<RadioButton> buttons)
    {
        this.buttons = buttons;
        this.clear();

        int row = 0;
        int col = 0;
        for (RadioButton btn : buttons) {
            btn.setContentDisplay(display);
            btn.setMnemonicParsing(mnemonicParsing);

            btn.setToggleGroup(group);

            add(btn, col, row);

            if (col < columns || columns == -1) {
                col++;
            } else {
                col = 0;
                row++;
            }
        }

    }

    public void setButtonsByLabel(List<String> labels)
    {
        setButtons(labels.stream().map(l -> {
            RadioButton btn = new RadioButton();
            btn.setText(l);
            return btn;
        }).collect(Collectors.toList()));
    }

    private void clear()
    {
        this.getChildren().clear();
        this.getRowConstraints().clear();
        this.getColumnConstraints().clear();
    }

    public ContentDisplay getDisplay()
    {
        return display;
    }

    public void setDisplay(ContentDisplay display)
    {
        this.display = display;
    }

    public boolean getMnemonicParsing()
    {
        return mnemonicParsing;
    }

    public void setMnemonicParsing(boolean mnemonicParsing)
    {
        this.mnemonicParsing = mnemonicParsing;
    }

    public int getColumns()
    {
        return columns;
    }

    public void setColumns(int columns)
    {
        this.columns = columns;
    }

    public int getRows()
    {
        return rows;
    }

    public void setRows(int rows)
    {
        this.rows = rows;
    }

    public String getToggled()
    {
        return toggled.get();
    }

    public void setToggled(String toggled)
    {
        this.toggled.set(toggled);
    }

    public StringProperty toggledProperty()
    {
        return toggled;
    }

}
