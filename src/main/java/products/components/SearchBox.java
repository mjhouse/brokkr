package products.components;

import core.communication.interfaces.Loggable;
import core.database.DatabaseManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import products.models.Product;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ResourceBundle;

public class SearchBox extends GridPane implements Loggable, Initializable {

    @FXML private ComboBox<String> combobox;

    @FXML private Button search;

    @FXML private Button left;

    @FXML private Button right;

    private static String DEFAULT = "*";

    private static String PATH = "/views/components/SearchBox.fxml";

    private SearchWindow.SearchResultCallback callback;

    private WeakReference<Product> lastResult;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        combobox.setOnKeyPressed(event -> {
            String text = combobox.getEditor().getText();
            if(event.getCode() == KeyCode.ENTER){
                combobox.setValue(text);
                search.fire();
            }
        });

        search.setOnAction(this::onSearch);

        left.setOnAction(this::onPrevious);
        right.setOnAction(this::onNext);
    }

    public SearchBox(){
        internalConstructor(null);
    }

    public SearchBox(SearchWindow.SearchResultCallback callback){
        internalConstructor(callback);
    }

    private void internalConstructor(SearchWindow.SearchResultCallback callback){
        FXMLLoader loader = new FXMLLoader(getClass().getResource(PATH));

        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            severe("Failed to load FXML file due to IOException",e);
            throw new RuntimeException(e);
        }

        setCallback(callback);
        lastResult = new WeakReference<>(null);
    }

    private void onNext(ActionEvent actionEvent)
    {
        Product last = lastResult.get();
        if(last != null && callback != null){
            last = DatabaseManager.getInstance().queryNext(last.getName());
            if(last != null){
                callback.onSearchComplete(last);
                lastResult = new WeakReference<>(last);
            }
        }
    }

    private void onPrevious(ActionEvent actionEvent)
    {
        Product last = lastResult.get();
        if(last != null && callback != null){
            last = DatabaseManager.getInstance().queryPrevious(last.getName());
            if(last != null){
                callback.onSearchComplete(last);
                lastResult = new WeakReference<>(last);
            }
        }
    }

    private void onSearch(ActionEvent actionEvent)
    {
        String text = combobox.getValue();

        if(text == null || text.isEmpty())
            text = DEFAULT;

        if(!combobox.getItems().contains(text))
            combobox.getItems().add(text);

        SearchWindow window = new SearchWindow(text);
        window.setCallback(this::onSearchComplete);

        window.show();
    }

    private void onSearchComplete(Product product)
    {
        lastResult = new WeakReference<>(product);
        callback.onSearchComplete(product);
    }

    public void setCallback(SearchWindow.SearchResultCallback callback){
        this.callback = callback;
    }

}
