package products.components;

import core.communication.interfaces.Loggable;
import core.models.BaseModel;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;
import javafx.util.Pair;

import java.lang.reflect.Method;
import java.util.List;

/**
 * A custom control that derives from ComboBox and adds additional functionality.
 *
 * @param <T> the type of object to display.
 */
public class AutoComboBox<T extends BaseModel> extends ComboBox<T> implements Loggable {

    private final Class<T> objectClass;
    private int currentPage = 0;

    private String accessor = "toString";

    private DataSource source = null;
    private ListView<T> listView = null;
    private String filter = "";

    /**
     * Empty constructor. Will create listeners for all relevant events,
     * override the default convertor for the base ComboBox and begin listening
     * to key presses.
     *
     * @param cls The class of the displayed object
     */
    public AutoComboBox(Class<T> cls)
    {
        super();

        objectClass = cls;

        // default to editable and set a cell factory
        setEditable(true);
        setVisibleRowCount(5);
        setCellFactory(this::getListCell);

        // set handlers
        setOnHidden(this::handleOnHiding);
        getEditor().setOnKeyPressed(this::handleOnKeyPressed);

        getSelectionModel().selectedIndexProperty().addListener(this::handleOnSelect);


        // set the converter for selected items
        setConverter(new StringConverter<>() {
            @Override
            public String toString(T object)
            {
                if (object != null)
                    return getMethodValue(object, accessor);
                return null;
            }

            @Override
            public T fromString(String string)
            {
                return getItems().stream().filter(ap -> getMethodValue(ap, accessor).equals(string)).findFirst().orElse(null);
            }
        });
    }

    /**
     * Handler that reacts to the user selecting a new option.
     *
     * @param obs the property being observed.
     * @param o   the old value.
     * @param n   the new value.
     */
    @SuppressWarnings("unused")
    private void handleOnSelect(ObservableValue<? extends Number> obs, Number o, Number n)
    {
        ObservableList<T> items = getItems();

        int percent = (items.size() / 100) * 80;
        int current = n.intValue();

        if (current > percent) {
            currentPage++;
            addItems(filter);
        }
    }

    /**
     * Handler that reacts to the scroll event.
     *
     * @param obs the property being observed.
     * @param o   the old value.
     * @param n   the new value.
     */
    @SuppressWarnings("unused")
    private void handleOnScroll(ObservableValue<? extends Number> obs, Number o, Number n)
    {
        //        boolean selection = (getSelectionModel().getSelectedIndex() < getItems().size() - 1);
        //        boolean position  = (n.doubleValue() == verticalBar.getMax());
        //        if( position && !selection ){
        //            currentPage++;
        //            addItems(filter);
        //        }
    }

    /**
     * Handler that reacts to the dropdown being hidden.
     *
     * @param event the event object.
     */
    @SuppressWarnings("unused")
    private void handleOnHiding(Event event)
    {
        final T item = getSelectionModel().getSelectedItem();
        if (item != null) {
            setSelected(item);
        }
    }

    /**
     * Handler that reacts to key presses within the textfield.
     *
     * @param event the event object.
     */
    private void handleOnKeyPressed(KeyEvent event)
    {
        KeyCode code = event.getCode();

        if (code.isLetterKey() || code.isDigitKey()) {
            filter = getEditor().getText().toLowerCase() + event.getText();
        }

        if (code == KeyCode.DOWN) {
            int index = getSelectionModel().getSelectedIndex();
            if (index > 0) {
                setSelected(index + 1);
            } else {
                setSelected(0);
            }
        } else if (code == KeyCode.BACK_SPACE) {
            filter = "";
            setValue(null);
            resetItems();
        } else if (filter.length() == 0) {
            resetItems();
        } else {
            updateItems(filter);
        }

        show();
    }

    /**
     * Generate list cells to display items.
     *
     * @param view ListView for cells.
     * @return a ListCell<T> instance.
     */
    private ListCell<T> getListCell(ListView<T> view)
    {

        if (view != null && listView == null) {
            listView = view;
            ScrollBar verticalBar = (ScrollBar) view.lookup(".scroll-bar:vertical");
            verticalBar.valueProperty().addListener(this::handleOnScroll);
        }

        return new ListCell<>() {
            @Override
            protected void updateItem(T item, boolean empty)
            {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(getMethodValue(item, accessor));
                } else {
                    setText(null);
                }
            }
        };
    }

    /**
     * Call an unknown method with signature: 'String method()'
     *
     * @param item the object to call the method on.
     * @param name the name of the method to call.
     * @return the string result of the method.
     */
    private String getMethodValue(T item, String name)
    {
        Method method;
        String result = "";
        try {
            method = item.getClass().getMethod(name);
            result = (String) method.invoke(item);
        } catch (Exception e) {
            severe("could not access method or method returned non-string object", e);
        }
        return result;
    }

    /**
     * Add items to the combobox with a given filter.
     *
     * @param filter a search term to filter on.
     */
    private void addItems(String filter)
    {
        ObservableList<T> items = getItems();
        DataSource data = getDataSource();

        if (data != null) {
            filter = (filter == null) ? "" : filter;
            int currentMult = 20;
            for (T item : data.query(objectClass, currentPage * currentMult, currentMult, new Pair<>("name", filter))) {
                if (!items.contains(item)) {
                    items.add(item);
                }
            }
        }
    }

    /**
     * Clear all items and get a new collection with the given
     * filter.
     *
     * @param filter the filter to select items with.
     */
    private void updateItems(String filter)
    {
        currentPage = 0;
        getItems().clear();
        addItems(filter);
    }

    /**
     * Clear current items and set the default collection.
     */
    private void resetItems()
    {
        currentPage = 0;
        getItems().clear();
        addItems(null);
    }

    /**
     * Select an item in the list view.
     *
     * @param item the item to select
     */
    private void setSelected(T item)
    {
        String value = getMethodValue(item, accessor);
        getSelectionModel().select(item);
        getEditor().positionCaret(value.length());
    }

    /**
     * Select an item in the list view by index.
     *
     * @param idx the index to select
     */
    @SuppressWarnings("SameParameterValue")
    private void setSelected(int idx)
    {
        T item = getItems().get(idx);
        setSelected(item);
    }

    /**
     * Get the key used to fetch a String value from the listed objects.
     *
     * @return the current accessor.
     */
    @SuppressWarnings("unused")
    public String getAccessor()
    {
        return accessor;
    }

    /**
     * Set the key used to fetch a display value from the listed objects.
     *
     * @param accessor the value to use as accessor.
     */
    public void setAccessor(String accessor)
    {
        this.accessor = accessor;
    }

    public AutoComboBox<T> withAccessor(String accessor){
        setAccessor(accessor);
        return this;
    }

    /**
     * Get the data source used to fill the ComboBox with entries.
     *
     * @return the DataSource used.
     */
    @SuppressWarnings("WeakerAccess")
    public DataSource getDataSource()
    {
        return source;
    }

    /**
     * Set the data source used to fill the ComboBox with entries.
     *
     * @param source the DataSource to use.
     */
    public void setDataSource(DataSource source)
    {
        this.source = source;
        if (getItems().isEmpty()) {
            addItems(null);
        }
    }

    public AutoComboBox<T> withSource(DataSource source){
        setDataSource(source);
        return this;
    }

    public AutoComboBox<T> withHandler(EventHandler<ActionEvent> handler){
        setOnAction(handler);
        return this;
    }

    /**
     * Interface defines a pageable source of data to
     * display in the ComboBox.
     */
    public interface DataSource {
        <T extends BaseModel> List<T> query(Class<T> cls, Integer start, Integer count, Pair<String,Object>... patterns);
    }

}
