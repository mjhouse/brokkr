package products.components;

import core.communication.interfaces.Loggable;
import core.components.Dialog;
import core.components.DynamicTable;
import core.components.IntegerField;
import core.database.DatabaseManager;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Pair;
import products.models.Product;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.entry;

public class SearchWindow extends Dialog implements Loggable {

    public interface SearchResultCallback<T> {
        void onSearchComplete(Product object);
    }

    private SearchResultCallback callback;

    @FXML private ComboBox<String> fields;

    @FXML private TextField filter;

    @FXML private Button update;

    @FXML private DynamicTable<Product> results;

    @FXML private CheckBox revisions;

    @FXML private Button previous;

    @FXML private IntegerField count;

    @FXML private Button next;

    private static Map<String, String[]> mapping = Map.ofEntries(
            entry("Name",new String[]{"name"}),
            entry("Author",new String[]{"author"}),
            entry("Description",new String[]{"description"}),
            entry("Class",new String[]{"productClass.code","productClass.description"}),
            entry("Group",new String[]{"productGroup.name"}),
            entry("Status",new String[]{"productStatus.name"}),
            entry("Source",new String[]{"productSource.source"}),
            entry("Created",new String[]{"created"})
    );

    private int page;

    private static int DEFAULT_LIMIT = 10;

    private static int DEFAULT_START = 0;

    private static String TITLE = "Search";

    private static String PATH = "/views/components/SearchWindow.fxml";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        update.setOnAction(this::onUpdate);
        results.ignoreProperties("id");

        Platform.runLater(() -> filter.requestFocus());
        filter.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.ENTER)
                update.fire();
        });

        previous.setOnAction(this::onPrevious);
        next.setOnAction(this::onNext);

        count.focusedProperty().addListener((obs,o,n) -> {
            if(o && !n) getPage();
        });

        count.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.ENTER){
                results.requestFocus();
            }
        });

        revisions.setOnAction(event -> {
            getPage();
        });
    }

    public SearchWindow()
    {
        super(TITLE, PATH);
        internalConstructor(null);
    }

    public SearchWindow(String search){
        super(TITLE, PATH);
        internalConstructor(search);
    }

    private void internalConstructor(String presearch)
    {
        page = DEFAULT_START;
        count.setValue(DEFAULT_LIMIT);
        setResizable(true);

        addButton("Cancel", ButtonBar.ButtonData.RIGHT, this::onCancel);
        addButton("View", ButtonBar.ButtonData.RIGHT, this::onView);

        if(presearch != null){
            filter.setText(presearch);
            update.fire();
        }
    }

    private void onPrevious(ActionEvent actionEvent)
    {
        int current = page;
        Integer step = count.getValue();

        if(step == null)
            count.setValue(step = DEFAULT_LIMIT);

        if((page - step) < 0){
            page = 0;
        }
        else {
            page -= step;
        }

        if(page != current)
            getPage();
    }

    private void onNext(ActionEvent actionEvent)
    {
        int current = page;
        Integer step = count.getValue();

        if(step == null)
            count.setValue(step = DEFAULT_LIMIT);

        if(results.getItems().size() < step)
            return;

        page += step;

        if(page != current)
            getPage();
    }

    public void setCallback(SearchResultCallback callback){
        this.callback = callback;
    }

    private void onUpdate(ActionEvent actionEvent)
    {
        getPage();
    }

    private void getPage(){
        final String value = filter.getText();
        Integer limit = count.getValue();

        if(value == null || value.isEmpty())
            return;

        if(limit == null){
            count.setValue(limit = DEFAULT_LIMIT); // set a default here
        }

        String[] keys = mapping.get(fields.getValue());

        List<Pair<String,Object>> pairs = Arrays.stream(keys)
                .map(key -> new Pair<String,Object>(key,value))
                .collect(Collectors.toCollection(ArrayList::new));

        List<Product> products = DatabaseManager
                .getInstance()
                .query(Product.class,page,limit,pairs);

        if(!revisions.isSelected()){
            products = products.stream().filter(Product::isCurrent).collect(Collectors.toList());
        }

        results.setContent(products);
    }

    private void onCancel(ActionEvent actionEvent)
    {
        close();
    }

    private void onView(ActionEvent actionEvent)
    {
        Product product = results.getSelectionModel().getSelectedItem();
        if(product != null && callback != null)
            callback.onSearchComplete(product);
        this.close();
    }


}
