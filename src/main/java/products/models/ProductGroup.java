package products.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.StringProperty;
import javafx.util.StringConverter;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "product_group", schema = "public")
@Access(value = AccessType.PROPERTY)
@SequenceGenerator(name = "product_group_gen", sequenceName = "product_group_id_seq", allocationSize = 1)
public class ProductGroup extends BaseModel {

    public static class ClassConverter extends StringConverter<ProductGroup> {

        private List<ProductGroup> classes;

        public void setClasses(List<ProductGroup> classes){
            this.classes = classes;
        }

        @Override
        public String toString(ProductGroup object)
        {
            return object != null ? object.getName() : null;
        }

        @Override
        public ProductGroup fromString(String string)
        {
            if(classes == null)
                return null;

            return classes.stream()
                    .filter(p -> p.getName().equals(string))
                    .findFirst().orElse(null);
        }
    }

    public static final ClassConverter STRING_CONVERTER = new ClassConverter();

    private final StringProperty code;

    private final StringProperty name;

    private final StringProperty description;

    public ProductGroup()
    {
        super();
        code = new StringProperty("");
        name = new StringProperty("");
        description = new StringProperty("");
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(ProductGroup.class,getId()) != null;
    }

    @Override
    public String stringValue(){
        return getName();
    }

    public void cloneModel(BaseModel other){

        if(other == null){
            resetModel();
            return;
        }

        if(!(other instanceof ProductGroup))
            throw new IllegalArgumentException("BaseModel other is not ProductGroup");

        ProductGroup model = (ProductGroup)other;

        setId(model.getId());
        setCode(model.getCode());
        setName(model.getName());
        setDescription(model.getDescription());
    }

    public void resetModel(){
        id.reset();
        code.reset();
        name.reset();
        description.reset();
    }

    //region helpers

    @Transient
    public static List<ProductGroup> getAll(){
        return DatabaseManager.getInstance().query(ProductGroup.class);
    }

    //endregion helpers

    //region properties

    //region code

    @Column(name="code")
    public String getCode()
    {
        return code.get();
    }

    @Transient
    public StringProperty codeProperty()
    {
        return code;
    }

    public void setCode(String code)
    {

        this.code.set(trim(code));
    }

    //endregion code

    //region name

    @Column(name="name")
    public String getName()
    {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(trim(name));
    }

    //endregion name

    //region description

    @Column(name="description")
    public String getDescription()
    {
        return description.get();
    }

    @Transient
    public StringProperty descriptionProperty()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description.set(trim(description));
    }

    //endregion description

    //endregion properties

}
