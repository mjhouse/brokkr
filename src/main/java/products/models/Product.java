package products.models;


import core.database.DatabaseManager;
import core.helpers.DateTimeHelper;
import core.models.BaseModel;
import core.models.properties.*;
import javafx.beans.property.Property;
import javafx.util.StringConverter;
import javafx.util.Pair;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * A business product that is stored in the database.
 */
@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "product", schema = "public")
@Access(value = AccessType.PROPERTY)
public class Product extends BaseModel {

    public static class ClassConverter extends StringConverter<Product> {

        private List<Product> classes;

        public void setClasses(List<Product> classes){
            this.classes = classes;
        }

        @Override
        public String toString(Product object)
        {
            return object != null ? object.getName() : null;
        }

        @Override
        public Product fromString(String string)
        {
            if(classes == null)
                return null;

            return classes.stream()
                    .filter(p -> p.getName().equals(string))
                    .findFirst().orElse(null);
        }
    }

    public static final ClassConverter STRING_CONVERTER = new ClassConverter();


    private final IntegerProperty productId;
    private final StringProperty name;
    private final BooleanProperty current;
    private final StringProperty author;
    private final StringProperty description;
    private final StringProperty units;
    private final DoubleProperty weight;
    private final BooleanProperty backflush;
    private final StringProperty created;

    private final ModelProperty<ProductClass> productClass;
    private final ModelProperty<ProductGroup> productGroup;
    private final ModelProperty<ProductSource> productSource;
    private final ModelProperty<ProductStatus> productStatus;

    /**
     * Create an empty product instance.
     */
    public Product()
    {
        super();

        productId = new IntegerProperty(this,"Product ID",0);
        name = new StringProperty(this,"Name","");
        current = new BooleanProperty(this,"Current",false);
        author = new StringProperty(this,"Author","");
        description = new StringProperty(this,"Description","");
        units = new StringProperty(this,"Units","");
        weight = new DoubleProperty(this,"Weight",0.0);
        backflush = new BooleanProperty(this,"Backflush",false);
        created = new StringProperty(this,"Created","");

//        productClass = new ProductClass();
        ////        productGroup = new ProductGroup();
        ////        productSource = new ProductSource();
        ////        productStatus = new ProductStatus();
        ////
        ////        new ModelProperty(this, "Class", productClass);
        ////        new ModelProperty(this, "Group", productGroup);
        ////        new ModelProperty(this, "Source", productSource);
        ////        new ModelProperty(this, "Status", productStatus);

        productClass = new ModelProperty<>(this, "Class", new ProductClass());
        productGroup = new ModelProperty<>(this, "Group", new ProductGroup());
        productSource = new ModelProperty<>(this, "Source", new ProductSource());
        productStatus = new ModelProperty<>(this, "Status", new ProductStatus());

    }

    public void cloneModel(BaseModel other){

        if(!(other instanceof Product))
            throw new IllegalArgumentException("BaseModel is not Product");

        Product model = (Product)other;

        setId(model.getId());
        setProductId(model.getProductId());
        setName(model.getName());
        setCurrent(model.isCurrent());
        setAuthor(model.getAuthor());
        setDescription(model.getDescription());
        setUnits(model.getUnits());
        setWeight(model.getWeight());
        setBackflush(model.isBackflush());
        setCreated(model.getCreated());

        setProductClass(model.getProductClass());
        setProductGroup(model.getProductGroup());
        setProductSource(model.getProductSource());
        setProductStatus(model.getProductStatus());
    }

    public void resetModel(){
        id.reset();
        productId.reset();
        name.reset();
        current.reset();
        author.reset();
        description.reset();
        units.reset();
        weight.reset();
        backflush.reset();
        created.reset();

        productClass.reset();
        productGroup.reset();
        productSource.reset();
        productStatus.reset();
    }

    //region helpers

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(Product.class,getId()) != null;
    }

    @Transient
    public static List<Product> getAll(){
        return DatabaseManager.getInstance().query(Product.class,
                                new Pair<>("current",true));
    }

    @Transient
    public List<Product> getAllRevisions(){
        return DatabaseManager.getInstance()
                .query(Product.class, new Pair<>("productId",getProductId()));
    }

    @Transient
    public Product getCurrent(){
        return DatabaseManager.getInstance()
                .query(Product.class,
                       new Pair<>("productId",getProductId()),
                       new Pair<>("current",true))
                .stream()
                .filter(p -> !p.equals(this))
                .findFirst()
                .orElse(null);
    }

    //endregion helpers

    //region properties

    //region product_id

    @Column(name="product_id")
    public Integer getProductId()
    {
        return productId.get();
    }

    @Transient
    public IntegerProperty productIdProperty()
    {
        return productId;
    }

    public void setProductId(Integer productId)
    {
        this.productId.set(productId);
    }

    //endregion product_id

    //region revision

    @Transient
    public Integer getRevision()
    {
        return getId();
    }

    public IntegerProperty revisionProperty()
    {
        return idProperty();
    }

    public void setRevision(Integer revision)
    {
        setId(revision);
    }

    //endregion revision

    //region name

    @Column(name="name")
    public String getName()
    {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(trim(name));
    }

    //endregion name

    //region current

    @Column(name="current")
    public boolean isCurrent()
    {
        return current.get();
    }

    @Transient
    public BooleanProperty currentProperty()
    {
        return current;
    }

    public void setCurrent(boolean current)
    {
        this.current.set(current);
    }

    //endregion current

    //region author

    @Column(name="author")
    public String getAuthor()
    {
        return author.get();
    }

    @Transient
    public StringProperty authorProperty()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author.set(trim(author));
    }

    //endregion author

    //region description

    @Column(name="description")
    public String getDescription()
    {
        return description.get();
    }

    @Transient
    public StringProperty descriptionProperty()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description.set(trim(description));
    }

    //endregion description

    //region units

    @Column(name="units")
    public String getUnits()
    {
        return units.get();
    }

    @Transient
    public StringProperty unitsProperty()
    {
        return units;
    }

    public void setUnits(String units)
    {
        this.units.set(trim(units));
    }

    //endregion units

    //region weight

    @Column(name="weight")
    public Double getWeight()
    {
        return weight.get();
    }

    @Transient
    public DoubleProperty weightProperty()
    {
        return weight;
    }

    public void setWeight(Double weight)
    {
        this.weight.set(weight);
    }

    //endregion weight

    //region backflush

    @Column(name="backflush")
    public boolean isBackflush()
    {
        return backflush.get();
    }

    @Transient
    public BooleanProperty backflushProperty()
    {
        return backflush;
    }

    public void setBackflush(boolean backflush)
    {
        this.backflush.set(backflush);
    }

    //endregion backflush

    //region created

    @Column(name="created")
    public String getCreated()
    {
        return created.get();
    }

    @Transient
    public LocalDateTime getCreatedDateTime(){
        return DateTimeHelper.getLocalDateTime(getCreated());
    }

    @Transient
    public StringProperty createdProperty()
    {
        return created;
    }

    public void setCreated(String created)
    {
        this.created.set(trim(created));
    }

    //endregion created

    //region class

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "class_fk")
    public ProductClass getProductClass()
    {
        return productClass.get();
    }

    public ModelProperty<ProductClass> productClassProperty()
    {
        return productClass;
    }

    public void setProductClass(ProductClass productClass)
    {
        this.productClass.set(productClass);
    }

    //endregion class

    //region group

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_fk")
    public ProductGroup getProductGroup()
    {
        return productGroup.get();
    }

    public ModelProperty<ProductGroup> productGroupProperty()
    {
        return productGroup;
    }

    public void setProductGroup(ProductGroup productGroup)
    {
        this.productGroup.set(productGroup);
    }

    //endregion group

    //region source

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "source_fk")
    public ProductSource getProductSource()
    {
        return productSource.get();
    }

    public ModelProperty<ProductSource> productSourceProperty()
    {
        return productSource;
    }

    public void setProductSource(ProductSource productSource)
    {
        this.productSource.set(productSource);
    }

    //endregion source

    //region status

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_fk")
    public ProductStatus getProductStatus()
    {
        return productStatus.get();
    }

    public ModelProperty<ProductStatus> productStatusProperty()
    {
        return productStatus;
    }

    public void setProductStatus(ProductStatus productStatus)
    {
        this.productStatus.set(productStatus);
    }

    //endregion status

    //region revisions

    @Transient
    public List<Product> getProductRevisions()
    {
        return DatabaseManager.getInstance().query(
                Product.class,new Pair<>("productId", getProductId()));
    }

    public void setProductRevisions(List<Product> revisions)
    {
        if(revisions.size() > 0)
            setProductId(revisions.get(0).getProductId());
    }

    public void setProductRevisions(Integer id)
    {
        setProductId(id);
    }

    //endregion revisions

    //endregion properties

}
