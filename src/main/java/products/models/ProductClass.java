package products.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.StringProperty;
import javafx.util.StringConverter;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "product_class", schema = "public")
@Access(value = AccessType.PROPERTY)
@SequenceGenerator(name = "product_class_gen", sequenceName = "product_class_id_seq", allocationSize = 1)
public class ProductClass extends BaseModel {

    public static class ClassConverter extends StringConverter<ProductClass> {
        private List<ProductClass> classes;

        public void setClasses(List<ProductClass> classes){
            this.classes = classes;
        }

        @Override
        public String toString(ProductClass object)
        {
            if(object != null){
                return object.getCode() + ": " + object.getDescription();
            }
            else {
                return null;
            }
        }

        @Override
        public ProductClass fromString(String string)
        {
            if(classes == null)
                return null;

            return classes.stream()
                    .filter(p -> {
                        String identifier = p.getCode() + ": " + p.getDescription();
                        return identifier.equals(string);
                    })
                    .findFirst().orElse(null);
        }
    }

    public static final ClassConverter STRING_CONVERTER = new ClassConverter();

    private final StringProperty code;

    private final StringProperty description;

    public ProductClass()
    {
        super();
        code = new StringProperty("");
        description = new StringProperty("");
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(ProductClass.class,getId()) != null;
    }

    @Override
    public String stringValue(){
        return description.get();
    }

    public void cloneModel(BaseModel other){

        if(other == null){
            resetModel();
            return;
        }

        if(!(other instanceof ProductClass))
            throw new IllegalArgumentException("BaseModel object is not ProductClass");

        ProductClass model = (ProductClass)other;

        setId(model.getId());
        setCode(model.getCode());
        setDescription(model.getDescription());

    }

    public void resetModel(){
        id.reset();
        code.reset();
        description.reset();
    }

    //region helpers

    @Transient
    public static List<ProductClass> getAll(){
        return DatabaseManager.getInstance().query(ProductClass.class);
    }

    //endregion helpers

    //region properties

    //region code

    @Column(name="code")
    public String getCode()
    {
        return code.get();
    }

    @Transient
    public StringProperty codeProperty()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code.set(trim(code));
    }

    //endregion code

    // region description

    @Column(name="description")
    public String getDescription()
    {
        return description.get();
    }

    @Transient
    public StringProperty descriptionProperty()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description.set(trim(description));
    }

    //endregion description

    //endregion properties

}
