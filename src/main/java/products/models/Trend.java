package products.models;

import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Trend {

    //region Fields

    private static final String NAME = "TREND";
    private static final String TAIL = "TAIL";
    private static final String LEAD = "LEAD";
    private static final String SUPPORTED_TREND = "trend-line-supported";
    private static final String UNSUPPORTED_TREND = "trend-line-unsupported";

    private AreaChart<Number, Number> chart;

    //endregion Fields

    //region Properties

    private ObjectProperty<XYChart.Series<Number, Number>> line;

    private double interval = 1200;

    //endregion Properties

    /**
     * The empty constructor.
     */
    public Trend()
    {
        line = new SimpleObjectProperty<>();
        line.addListener(this::onLineChange);
    }

    //region Private Methods

    /**
     * Get the chart data within the current bounds.
     *
     * @return an arraylist of data points
     */
    private ArrayList<double[]> getSourceData()
    {

        // get the xAxis
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();

        // get the bounds of the xAxis
        double start = xAxis.getLowerBound();
        double end = xAxis.getUpperBound();

        // look for a series with the name DATA
        for (XYChart.Series<Number, Number> series : chart.getData()) {

            if (series.getName().equals("DATA")) {

                // if it's found, filter for just the data inside
                // the bounds of the chart and then collect into an
                // ArrayList.
                return series.getData().stream().filter(d -> {
                    double x = d.getXValue().doubleValue();
                    return (x >= start && x <= end);
                }).map(d -> {
                    double x = d.getXValue().doubleValue();
                    double y = d.getYValue().doubleValue();
                    return new double[]{x, y};
                }).collect(Collectors.toCollection(ArrayList::new));
            }
        }

        // if the data wasn't found, return null
        return null;
    }

    /**
     * Removes existing and inserts a replacement data series.
     *
     * @param trend the series to add to the chart
     * @param name  the name of the series
     * @param css   the css class of the series
     */
    private void addLineToChart(XYChart.Series<Number, Number> trend, String name, String css)
    {
        trend.setName(name);

        if (chart != null) {
            chart.getData().removeIf(d -> name.equals(d.getName()));
            chart.getData().add(trend);

            trend.getNode().getStyleClass().add(css);
            chart.applyCss();
        }

    }

    //endregion Private Methods

    //region Handlers

    /**
     * Handles changes in the start/end point of the XYChart.
     *
     * @param obs the property being observed (lower/upper bound)
     */
    private void onBoundsChange(Observable obs)
    {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        ArrayList<double[]> data = getSourceData();

        if (data != null) {
            double[][] array = new double[data.size()][2];
            array = data.toArray(array);

            SimpleRegression linear = new SimpleRegression(true);
            linear.addData(array);

            XYChart.Series<Number, Number> lead = new XYChart.Series<>();
            XYChart.Series<Number, Number> tail = new XYChart.Series<>();
            XYChart.Series<Number, Number> trend = new XYChart.Series<>();

            double xStart = xAxis.getLowerBound();
            double xEnd = xAxis.getUpperBound();

            double xMin = -1;
            double xMax = -1;

            for (double[] point : data) {
                double x = point[0];

                if (xMin == -1 || x < xMin)
                    xMin = x;
                if (xMax == -1 || x > xMax)
                    xMax = x;

                double m = linear.getSlope();
                double b = linear.getIntercept();

                double ny = m * x + b;

                if (ny > 0)
                    trend.getData().add(new XYChart.Data<>(x, ny));
            }


            for (double x = xStart; x <= xMin; x += interval) {
                double prediction = linear.predict(x);
                if (prediction > 0) {
                    lead.getData().add(new XYChart.Data<>(x, prediction));
                }
            }

            for (double x = xMax; x <= xEnd; x += interval) {
                double prediction = linear.predict(x);
                if (prediction > 0) {
                    tail.getData().add(new XYChart.Data<>(x, prediction));
                }
            }

            addLineToChart(lead, LEAD, UNSUPPORTED_TREND);
            addLineToChart(tail, TAIL, UNSUPPORTED_TREND);

            setLine(trend);
        }
    }

    /**
     * Handles changes in the line generated by this Trend.
     *
     * @param obs the property being observed (line)
     */
    private void onLineChange(Observable obs)
    {
        XYChart.Series<Number, Number> trend = getLine();
        addLineToChart(trend, NAME, SUPPORTED_TREND);
    }

    //endregion Handlers

    //region Public Methods

    /**
     * Bind this trend to a given area chart.
     *
     * @param areaChart the chart to bindAll
     */
    public void bind(AreaChart<Number, Number> areaChart)
    {
        if (chart != null)
            unBind();

        chart = areaChart;
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();

        xAxis.lowerBoundProperty().addListener(this::onBoundsChange);
        xAxis.upperBoundProperty().addListener(this::onBoundsChange);

        onBoundsChange(null);
    }

    /**
     * Unbind from the current chart.
     */
    public void unBind()
    {
        if (chart != null) {
            NumberAxis xAxis = (NumberAxis) chart.getXAxis();

            xAxis.lowerBoundProperty().removeListener(this::onBoundsChange);
            xAxis.upperBoundProperty().removeListener(this::onBoundsChange);

            chart.getData().removeIf(d -> (NAME.equals(d.getName()) || LEAD.equals(d.getName()) || TAIL.equals(d.getName())));

            chart = null;
        }
    }

    /**
     * Get the trend line property object.
     *
     * @return the ObjectProperty of the trend series.
     */
    public ObjectProperty<XYChart.Series<Number, Number>> lineProperty()
    {
        return line;
    }

    /**
     * Get the data of the trend line.
     *
     * @return the data series of the trend line
     */
    public XYChart.Series<Number, Number> getLine()
    {
        return line.get();
    }

    /**
     * Set the trend line data and update the chart.
     *
     * @param trend the data series to set as the trend line.
     */
    public void setLine(XYChart.Series<Number, Number> trend)
    {
        line.set(trend);
    }

    /**
     * Get the tick interval for the trend.
     *
     * @return the tick interval
     */
    public double getTick()
    {
        return interval;
    }

    /**
     * Set the tick interval for the trend.
     *
     * @param tick the new tick interval
     */
    public void setTick(double tick)
    {
        this.interval = tick;
    }

    //endregion Public Methods

}
