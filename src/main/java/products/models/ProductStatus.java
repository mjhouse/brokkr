package products.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.StringProperty;
import javafx.util.StringConverter;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "product_status", schema = "public")
@Access(value = AccessType.PROPERTY)
@SequenceGenerator(name = "product_status_gen", sequenceName = "product_status_id_seq", allocationSize = 1)
public class ProductStatus extends BaseModel {

    public static class ClassConverter extends StringConverter<ProductStatus> {

        private List<ProductStatus> classes;

        public void setClasses(List<ProductStatus> classes){
            this.classes = classes;
        }

        @Override
        public String toString(ProductStatus object)
        {
            return object != null ? object.getName() : null;
        }

        @Override
        public ProductStatus fromString(String string)
        {
            if(classes == null)
                return null;

            return classes.stream()
                    .filter(p -> p.getName().equals(string))
                    .findFirst().orElse(null);
        }
    }

    public static final ClassConverter STRING_CONVERTER = new ClassConverter();

    private final StringProperty name;

    public ProductStatus()
    {
        super();
        name = new StringProperty("");
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(ProductStatus.class,getId()) != null;
    }

    @Override
    public String stringValue(){
        return getName();
    }

    public void cloneModel(BaseModel other){

        if(other == null){
            resetModel();
            return;
        }

        if(!(other instanceof ProductStatus))
            throw new IllegalArgumentException("BaseModel other is not ProductStatus");

        ProductStatus model = (ProductStatus) other;

        setId(model.getId());
        setName(model.getName());
    }

    public void resetModel(){
        id.reset();
        name.reset();
    }

    //region helpers

    @Transient
    public static List<ProductStatus> getAll(){
        return DatabaseManager.getInstance().query(ProductStatus.class);
    }

    //endregion helpers

    //region properties

    //region name

    @Column(name="name")
    public String getName()
    {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(trim(name));
    }

    //endregion name

    //endregion properties

}
