package products.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.StringProperty;
import javafx.util.StringConverter;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "product_source", schema = "public")
@Access(value = AccessType.PROPERTY)
public class ProductSource extends BaseModel {

    public static class ClassConverter extends StringConverter<ProductSource> {

        private List<ProductSource> classes;

        public void setClasses(List<ProductSource> classes){
            this.classes = classes;
        }

        @Override
        public String toString(ProductSource object)
        {
            return object != null ? object.getSource() : null;
        }

        @Override
        public ProductSource fromString(String string)
        {
            if(classes == null)
                return null;

            return classes.stream()
                    .filter(p -> p.getSource().equals(string))
                    .findFirst().orElse(null);
        }
    }

    public static final ClassConverter STRING_CONVERTER = new ClassConverter();

    private final StringProperty source;

    public ProductSource()
    {
        super();
        source = new StringProperty("");
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(ProductSource.class,getId()) != null;
    }

    @Override
    public String stringValue(){
        return getSource();
    }

    public void cloneModel(BaseModel other){

        if(other == null){
            resetModel();
            return;
        }

        if(!(other instanceof ProductSource))
            throw new IllegalArgumentException("BaseModel other is not ProductSource");

        ProductSource model = (ProductSource) other;

        setId(model.getId());
        setSource(model.getSource());

    }

    public void resetModel(){
        id.reset();
        source.reset();
    }

    //region helpers

    @Transient
    public static List<ProductSource> getAll(){
        return DatabaseManager.getInstance().query(ProductSource.class);
    }

    //endregion helpers

    //region properties

    //region source

    @Column(name="source")
    public String getSource()
    {
        return source.get();
    }

    @Transient
    public StringProperty sourceProperty()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source.set(trim(source));
    }

    //endregion source

    //endregion properties

}
