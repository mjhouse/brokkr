package products;

import core.components.GraphicButton;
import core.components.TabWindow;
import core.controllers.BaseController;
import core.database.DatabaseManager;
import core.helpers.TransientHelper;
import core.models.collections.MenuCollection;
import core.models.collections.WidgetCollection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import products.components.*;
import products.models.ProductStatus;
import products.models.Trend;
import products.models.Product;
import products.models.ProductSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The controller for the Products workspace.
 */
public class ProductsController extends BaseController {

    public TextField revisionField;

    public TextField createdDate;

    public TextField authorField;

    public ComboBox<ProductStatus> statusField;

    public TextField classField;

    public CheckBox currentRevision;

    @FXML
    private TabPane tabPane;

    /**
     * The product ID field.
     */
    @FXML
    private TextField productID;

    /**
     * The MRP Classification field.
     */
    @SuppressWarnings("unused")
    @FXML
    private ToggleGrid mrpClass;

    /**
     * The description field.
     */
    @FXML
    private TextArea productDescription;

    /**
     * The combo box used to select products by product id.
     */
    private SearchBox products;

    /**
     * The chart that shows product usage.
     */
    @FXML
    private AutoTrendChart usageChart;

    @FXML
    private TableView<Product> revisionTable;

    @FXML
    private TableColumn<Product,Integer> revisionNumberColumn;

    @FXML
    private TableColumn<Product,String> revisionCreatedColumn;

    @FXML
    private TableColumn<Product,String> revisionDescriptionColumn;

    private Product current;

    private ArrayList<Tab> tabs;

    private ObservableList<Product> revisionsList;

    public ProductsController(){
        super("/views/workspaces/products_workspace.fxml");
    }

    /**
     * Initial setup for the workspace.
     */
    public void initialize()
    {

        tabs = (ArrayList<Tab>) tabPane.getTabs().stream().map(t -> {
            Tab tab = new Tab();
            tab.setText(t.getText());

            AnchorPane pane = (AnchorPane) t.getContent().lookup("AnchorPane");
            tab.setContent(pane);

            return tab;
        }).collect(Collectors.toList());

        current = new Product();

        productID.textProperty().bindBidirectional(current.nameProperty());
        productDescription.textProperty().bindBidirectional(current.descriptionProperty());
        createdDate.textProperty().bindBidirectional(current.createdProperty());

        statusField.valueProperty().bindBidirectional(current.productStatusProperty());
//        statusField.setConverter(new StringConverter<ProductStatus>() {
//            @Override
//            public String toString(ProductStatus object)
//            {
//                return object.getName();
//            }
//
//            @Override
//            public ProductStatus fromString(String string)
//            {
//                return null;
//            }
//        });
        //statusField.textProperty().bindBidirectional(current.getProductStatus().nameProperty());

        revisionField.textProperty().bindBidirectional(current.revisionProperty(), new StringConverter<>() {
            @Override
            public String toString(Integer object)
            {
                if (object != null) {
                    return object.toString();
                } else {
                    return "";
                }
            }

            @Override
            public Integer fromString(String string)
            {
                return Integer.parseInt(string);
            }
        });

        usageChart.setData(TransientHelper.generateTestData());
        usageChart.setTrend(new Trend());

        authorField.textProperty().bindBidirectional(current.authorProperty());

        List<ProductSource> sources = DatabaseManager.getInstance().query(ProductSource.class);
        mrpClass.setButtonsByLabel(sources.stream().map(ProductSource::getSource).collect(Collectors.toList()));
        mrpClass.toggledProperty().bindBidirectional(current.getProductSource().sourceProperty());
        classField.textProperty().bindBidirectional(current.getProductClass().codeProperty());
        currentRevision.selectedProperty().bindBidirectional(current.currentProperty());

        revisionsList = FXCollections.observableArrayList();
        revisionTable.setItems(revisionsList);

        revisionNumberColumn.setCellValueFactory(data -> data.getValue().revisionProperty());
        revisionCreatedColumn.setCellValueFactory(data -> data.getValue().createdProperty());
        revisionDescriptionColumn.setCellValueFactory(data -> data.getValue().descriptionProperty());
        current.productIdProperty().addListener((obs,o,n) -> {
            revisionsList.setAll(current.getProductRevisions()
                                         .stream()
                                         .filter(p -> !p.getId().equals(current.getId()))
                                         .collect(Collectors.toList()));
            revisionTable.sort();
        });

        revisionTable.getSortOrder().add(revisionNumberColumn);

    }

    private void onPopOut(ActionEvent e)
    {
        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        TabWindow popup = new TabWindow(tab);
        popup.show();
    }

    private void createTab(int i)
    {
        if (i <= tabs.size()) {
            Tab tab = tabs.get(i);
            if (tabPane.getTabs().stream().noneMatch(t -> t.getText().equals(tab.getText()))) {
                // add the tab if there is no tab with the
                // same text in the tab label.
                tabPane.getTabs().add(i, tab);
            }
            tabPane.getSelectionModel().select(i);
        }
    }

    private void onNew(ActionEvent actionEvent)
    {
        CreateProductWindow w = new CreateProductWindow();
        w.show();
    }

    private void onEdit(ActionEvent actionEvent)
    {
        authorField.setDisable(false);
        statusField.setDisable(false);
        classField.setDisable(false);
        currentRevision.setDisable(false);
        productID.setDisable(false);
        mrpClass.setDisable(false);
        productDescription.setDisable(false);
    }

    private void onFinishEdit(){
        authorField.setDisable(true);
        statusField.setDisable(true);
        classField.setDisable(true);
        currentRevision.setDisable(true);
        productID.setDisable(true);
        mrpClass.setDisable(true);
        productDescription.setDisable(true);
    }

    private void onSave(ActionEvent actionEvent)
    {
        if(current != null && current.inDatabase()){
            DatabaseManager.getInstance().save(current);
            onFinishEdit();
        }
    }

    private void onDelete(ActionEvent actionEvent)
    {
        if(current != null && current.inDatabase()){
            boolean delete = confirm(
                    "Delete Product",
                    current.getName() + " will be DELETED PERMANENTLY",
                    "Are you sure you want to do this?");

            if(delete){
                current.delete();
                current.resetModel();
            }
        }
    }

    @Override
    public ArrayList<Menu> getMenuCollection()
    {
        MenuCollection menus = new MenuCollection()
                .withMenu("Products")
                .withItem("New",this::onNew)
                .withItem("Edit",this::onEdit)
                .withItem("Delete",this::onDelete)
                .withItem("Save",this::onSave)
                .withMenu("View");

        // withItem will attach a menuitem to the last menu created with menu
        for (int i = 0; i < tabs.size(); ++i) {
            Tab tab = tabs.get(i);
            int idx = i;
            menus.withItem(tab.getText(),e -> createTab(idx));
        }

        return menus;
    }

    @Override
    public WidgetCollection getWidgetCollection()
    {
        return new WidgetCollection()
                .with(new GraphicButton("dashicons-external")
                    .onClick(this::onPopOut))
                .with(new SearchBox(product -> {
                    current.cloneModel(product);
                }));
    }

}
