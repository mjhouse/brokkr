package financial.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.DoubleProperty;
import core.models.properties.IntegerProperty;
import core.models.properties.StringProperty;

import javax.persistence.*;
import java.beans.Transient;

/**
 * A financial account that maintains credit and debit information
 */
@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "account", schema = "public")
@Access(value = AccessType.PROPERTY)
@SequenceGenerator(name = "account_gen", sequenceName = "account_id_seq", allocationSize = 1)
public class Account extends BaseModel {

    private final IntegerProperty code;

    private final StringProperty name;

    private final DoubleProperty balance;

    private AccountType type;

    public Account()
    {
        super();

        code = new IntegerProperty(this,"code",0);
        name = new StringProperty(this,"name","");
        balance = new DoubleProperty(this,"balance",0.0);

        type = new AccountType();
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(Account.class, getId()) != null;
    }

    public void cloneModel(BaseModel other){

        if(!(other instanceof Account))
            throw new IllegalArgumentException("BaseModel other is not Account");

        Account model = (Account)other;

        setId(model.getId());
        setCode(model.getCode());
        setBalance(model.getBalance());
        setType(model.getType());

    }

    public void resetModel(){
        id.reset();
        code.reset();
        balance.reset();

        type.resetModel();
    }

    //region properties

    //region code

    @Column(name="code")
    public int getCode()
    {
        return code.get();
    }

    @Transient
    public IntegerProperty codeProperty()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code.set(code);
    }

    //endregion code

    //region name

    @Column(name="name")
    public String getName()
    {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    //endregion name

    //region balance

    @Column(name="balance")
    public double getBalance()
    {
        return balance.get();
    }

    @Transient
    public DoubleProperty balanceProperty()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance.set(balance);
    }

    //endregion balance

    //region type

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_fk")
    public AccountType getType()
    {
        return type;
    }

    @Transient
    public void setType(AccountType type)
    {
        this.type.cloneModel(type);
    }

    //endregion type

    //region properties

}
