package financial.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.StringProperty;

import javax.persistence.*;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "company", schema = "public")
@Access(value = AccessType.PROPERTY)
@SequenceGenerator(name = "company_gen", sequenceName = "company_id_seq", allocationSize = 1)
public class Company extends BaseModel {

    private final StringProperty phone;

    private final StringProperty email;

    private final StringProperty fax;

    private final StringProperty address;

    private final StringProperty name;

    public Company()
    {
        super();

        phone = new StringProperty("");
        email = new StringProperty("");
        fax = new StringProperty("");
        address = new StringProperty("");
        name = new StringProperty("");
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(Company.class, getId()) != null;
    }

    public void cloneModel(BaseModel other){

        if(!(other instanceof Company))
            throw new IllegalArgumentException("BaseModel other is not Company");

        Company model = (Company)other;

        setId(model.getId());
        setPhone(model.getPhone());
        setEmail(model.getEmail());
        setFax(model.getFax());
        setAddress(model.getAddress());
        setName(model.getName());
    }

    public void resetModel(){
        id.reset();
        phone.reset();
        email.reset();
        fax.reset();
        address.reset();
        name.reset();
    }

    //region properties

    //region phone

    @Column(name="phone")
    public String getPhone()
    {
        return phone.get();
    }

    @Transient
    public StringProperty phoneProperty()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone.set(phone);
    }

    //endregion phone

    //region email

    @Column(name="email")
    public String getEmail()
    {
        return email.get();
    }

    @Transient
    public StringProperty emailProperty()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email.set(email);
    }

    //endregion email

    // region fax

    @Column(name="fax")
    public String getFax()
    {
        return fax.get();
    }

    @Transient
    public StringProperty faxProperty()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax.set(fax);
    }

    //endregion fax

    //region address

    @Column(name="address")
    public String getAddress()
    {
        return address.get();
    }

    @Transient
    public StringProperty addressProperty()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address.set(address);
    }

    //endregion address

    //region name

    @Column(name="name")
    public String getName()
    {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    //endregion name

    //endregion properties

}
