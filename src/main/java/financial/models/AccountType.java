package financial.models;

import core.database.DatabaseManager;
import core.models.BaseModel;
import core.models.properties.StringProperty;

import javax.persistence.*;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table(name = "account_type", schema = "public")
@Access(value = AccessType.PROPERTY)
@SequenceGenerator(name = "account_type_gen", sequenceName = "account_type_id_seq", allocationSize = 1)
public class AccountType extends BaseModel {

    private final StringProperty name;


    public AccountType()
    {
        super();
        name = new StringProperty("");
    }

    public void cloneModel(BaseModel other){

        if(!(other instanceof AccountType))
            throw new IllegalArgumentException("BaseModel other is not AccountType");

        AccountType model = (AccountType) other;

        setId(model.getId());
        setName(model.getName());
    }

    @Transient
    @Override
    public boolean inDatabase()
    {
        return DatabaseManager.getInstance().queryModel(AccountType.class, getId()) != null;
    }

    public void resetModel(){
        id.reset();
        name.reset();
    }

    //region properties

    //region name

    @Column(name="name")
    public String getName()
    {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    //endregion name

    //endregion properties

}
