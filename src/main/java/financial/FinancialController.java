package financial;

import core.controllers.BaseController;
import core.models.collections.WidgetCollection;
import javafx.scene.control.Menu;

import java.util.ArrayList;

public class FinancialController extends BaseController {

    public FinancialController(){
        super("/views/workspaces/financial_workspace.fxml");
    }

    /**
     * Initial setup for the workspace.
     */
    public void initialize()
    {
    }

    @Override
    public ArrayList<Menu> getMenuCollection()
    {
        return new ArrayList<>();
    }

    @Override
    public WidgetCollection getWidgetCollection()
    {
        return new WidgetCollection()
                .withButton("New",null);
    }

}
