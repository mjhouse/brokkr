package testing;

import core.database.DatabaseManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import settings.models.Settings;

import java.security.InvalidParameterException;

public class BaseTest {

    protected Settings settings;

    @BeforeClass
    public static void startClassBase() {

    }

    @AfterClass
    public static void endClassBase() {

    }

    @Before
    public void startTestBase() throws Exception {
        settings = Settings.getInstance("brokkr_test");
    }

    @After
    public void endTestBase() throws Exception {
        settings.deleteInstance();
    }

}
