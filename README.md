# Brokkr

#### Introduction

Brokkr is an enterprise resource management application tailored to 
small and medium-sized manufacturing companies of any kind. It's 
written in Java to make cross platform deployment, GUI design and
database interaction easier.

All code was written by Michael House. Outside libraries and 
resources:

| Name                     	| License                                                    	    | Supports Commercial  |
|--------------------------	|-----------------------------------------------------------------|----------------------|
| Hibernate                	| [LGPL-2.1](https://opensource.org/licenses/LGPL-2.1)         	| Must use GPL         |
| OpenJFX                  	| [GPL 2.0](https://opensource.org/licenses/GPL-2.0)           	| Must use GPL         |
| Ikonli                   	| [Apache-2.0](https://opensource.org/licenses/Apache-2.0)     	| Yes                  |
| Apache Commons Math      	| [Apache-2.0](https://opensource.org/licenses/Apache-2.0)     	| Yes                  |
| Apache Commons BeanUtils 	| [Apache-2.0](https://opensource.org/licenses/Apache-2.0)     	| Yes                  |
| PostgreSQL JDBC Driver   	| [BSD-2-Clause](https://opensource.org/licenses/BSD-2-Clause) 	| Yes                  |

#### Documentation

- For the code and API: [Docs](https://mjhouse.gitlab.io/brokkr/)
- Usage and tutorials: Usage
